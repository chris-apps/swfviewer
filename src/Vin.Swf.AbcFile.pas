unit Vin.Swf.AbcFile;

interface

uses
  SysUtils, Windows, Classes, Generics.Collections, ActiveX, msxml,
  vxUtils, vxXmlDom,
  Vin.Swf.Types, Vin.Swf.AbcFile.Consts, Vin.Swf.Tools, Vin.Swf.Tools2, xmldom;

type
  u8 = UInt8;
  u16 = UInt16;
  s24 = Int32;    // 3 字节
  u30 = UInt32;   // 可变长
  u32 = UInt32;   // 可变长
  s32 = Int32;    // 可变长
  d64 = Double;   // 8-byte IEEE-754

{$REGION 'Constant Pool'}

type
  TU30List = TList<u30>;
  TU8List = TList<u8>;

  TAbcNamespaceInfo = class
  private
    FKind: u8;
    FName: u30;
  public
    constructor Create;
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
    property Kind: u8 read FKind write FKind;
    property Name: u30 read FName write FName;
  end;

  TAbcNamespaceSetInfo = TU30List;

  // Multiname Kind
  TAbcMultinameObjectBase = class
  private
    FKind: u8;
  public
    constructor Create(AKind: u8); virtual;
    procedure ReadFromStream(ABE: TBitsEngine); virtual;
    procedure WriteToStream(ABE: TBitsEngine); virtual;
    property Kind: u8 read FKind write FKind;
  end;

  TAbcMN_QName = class(TAbcMultinameObjectBase)
  private
    FNS: u30;
    FName: u30;
  public
    procedure ReadFromStream(ABE: TBitsEngine); override;
    procedure WriteToStream(ABE: TBitsEngine); override;
    property NS: u30 read FNS write FNS;
    property Name: u30 read FName write FName;
  end;

  TAbcMN_RTQName = class(TAbcMultinameObjectBase)
  private
    FName: u30;
  public
    procedure ReadFromStream(ABE: TBitsEngine); override;
    procedure WriteToStream(ABE: TBitsEngine); override;
    property Name: u30 read FName write FName;
  end;

  TAbcMN_RTQNameL = class(TAbcMultinameObjectBase)
  end;

  TAbcMN_Multiname = class(TAbcMultinameObjectBase)
  private
    FName: u30;
    FNsSet: u30;
  public
    procedure ReadFromStream(ABE: TBitsEngine); override;
    procedure WriteToStream(ABE: TBitsEngine); override;
    property Name: u30 read FName write FName;
    property NsSet: u30 read FNsSet write FNsSet;
  end;

  TAbcMN_MultinameL = class(TAbcMultinameObjectBase)
  private
    FNsSet: u30;
  public
    procedure ReadFromStream(ABE: TBitsEngine); override;
    procedure WriteToStream(ABE: TBitsEngine); override;
    property NsSet: u30 read FNsSet write FNsSet;
  end;

  TAbcMN_GenericName = class(TAbcMultinameObjectBase)
  private
    FTypeDefinition: u30;
    FParams: TU30List;
  public
    constructor Create(AKind: u8); override;
    procedure ReadFromStream(ABE: TBitsEngine); override;
    procedure WriteToStream(ABE: TBitsEngine); override;
    property TypeDefinition: u30 read FTypeDefinition write FTypeDefinition;
    property Params: TU30List read FParams;
  end;

  TAbcConstantPool = class
  private
    FIntegers: TList<s32>;
    FUIntegers: TU30List;
    FDoubles: TList<d64>;
    FStrings: TList<AnsiString>;
    FNamespaces: TList<TAbcNamespaceInfo>;
    FNamespaceSets: TList<TAbcNamespaceSetInfo>;
    FMultinames: TList<TAbcMultinameObjectBase>;
  public
    constructor Create;
    procedure Clear;
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure SaveToXml(AParentNode: IXMLDOMNode);
    procedure WriteToStream(ABE: TBitsEngine);
    property Integers: TList<s32> read FIntegers;
    property UIntegers: TU30List read FUIntegers; // 实际上应该是 TList<u32>
    property Doubles: TList<d64> read FDoubles;
    property Strings: TList<AnsiString> read FStrings;
    property Namespaces: TList<TAbcNamespaceInfo> read FNamespaces;
    property NamespaceSets: TList<TAbcNamespaceSetInfo> read FNamespaceSets;
    property Multinames: TList<TAbcMultinameObjectBase> read FMultinames;
  end;

{$ENDREGION}

{$REGION 'Method Signature'}

type
  TAbcOptionalDetail = class
  private
    FVal: u30;
    FKind: u8;
  public
    property Val: u30 read FVal write FVal;
    property Kind: u8 read FKind write FKind;
  end;

  TAbcOptionals = TList<TAbcOptionalDetail>;

  TAbcMethodInfo = class
  private
    FReturnType: u30;
    FParamTypes: TU30List;
    FName: u30;
    FFlags: u8;
    FOptionals: TAbcOptionals;
    FParamNames: TU30List;
    function GetParamCount: u30;
  public
    constructor Create;
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
    property ParamCount: u30 read GetParamCount;
    property ReturnType: u30 read FReturnType write FReturnType;
    property ParamTypes: TU30List read FParamTypes;
    property Name: u30 read FName write FName;
    property Flags: u8 read FFlags write FFlags;
    property Optionals: TAbcOptionals read FOptionals;
    property ParamNames: TU30List read FParamNames;
  end;

  TAbcMethods = class(TList<TAbcMethodInfo>)
  public
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure SaveToXml(AParentNode: IXMLDOMNode);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

{$ENDREGION}

{$REGION 'Meta Data'}

type
  TAbcMetaitem = class
  private
    FKey: u30;
    FValue: u30;
  public
    property Key: u30 read FKey write FKey;
    property Value: u30 read FValue write FValue;
  end;

  TAbcMetadata = class
  private
    FName: u30;
    FItems: TList<TAbcMetaitem>;
  public
    constructor Create;
    property Name: u30 read FName write FName;
    property Items: TList<TAbcMetaitem> read FItems;
  end;

  TAbcMetadatas = class(TList<TAbcMetadata>)
  public
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

{$ENDREGION}

{$REGION 'Instance and Class Info'}

type
  TAbcTrait = class
  private
    FName: u30;
    FKindAndAttr: u8;
    FData: Pointer;
    FMetadatas: TU30List;
    FAttr: u8;
    FKind: u8;
    function GetKindAndAttr: u8;
    procedure SetKindAndAttr(const Value: u8);
  protected
  public
    constructor Create;
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
    property Name: u30 read FName write FName;
    property KindAndAttr: u8 read GetKindAndAttr write SetKindAndAttr;
    property Data: Pointer read FData write FData;
    property Metadatas: TU30List read FMetadatas;
    property Attr: u8 read FAttr write FAttr;
    property Kind: u8 read FKind write FKind;
  end;

  TTraitList = TList<TAbcTrait>;

  TAbcInstance = class
  private
    FName: u30;
    FSuperName: u30;
    FFlags: u8;
    FProtectedNs: u30;
    FInterfaces: TU30List;
    FIInit: u30;
    FTraits: TTraitList;
  public
    constructor Create;
    destructor Destroy; override;
    property Name: u30 read FName write FName;
    property SuperName: u30 read FSuperName write FSuperName;
    property Flags: u8 read FFlags write FFlags;
    property ProtectedNs: u30 read FProtectedNs write FProtectedNs;
    property Interfaces: TU30List read FInterfaces;
    property IInit: u30 read FIInit write FIInit;
    property Traits: TTraitList read FTraits;
  end;

  TAbcInstances = class(TList<TAbcInstance>)
  public
    procedure ReadFromStream(ABE: TBitsEngine; AInstCount: Int32);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

  TAbcTraitSlot = class
    SlotId: u30;
    TypeName: u30;
    VIndex: u30;
    VKind: u8;
  end;

  TAbcTraitClass = class
    SlotId: u30;
    Classi: u30;
  end;

  TAbcTraitFunction = class
    SlotId: u30;
    Function_: u30;
  end;

  TAbcTraitMethod = class
    DispId: u30;
    Method: u30;
  end;

  TAbcClassInfo = class
  private
    FTraits: TTraitList;
  public
    CInit: u30;
    constructor Create;
    destructor Destroy; override;
    property Traits: TTraitList read FTraits;
  end;

  TAbcClasses = class(TList<TAbcClassInfo>)
  public
    procedure ReadFromStream(ABE: TBitsEngine; ACount: Int32);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

{$ENDREGION}

{$REGION 'Scripts Info'}

type
  TAbcScript = class
  private
    FInit: u30;
    FTraits: TTraitList;
  public
    constructor Create;
    destructor Destroy; override;
    property Init: u30 read FInit write FInit;
    property Traits: TTraitList read FTraits;
  end;

  TAbcScripts = class(TList<TAbcScript>)
  public
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

{$ENDREGION}

{$REGION 'Method body'}

type
  TAbcException = class
    From: u30;
    To_: u30;
    Target: u30;
    ExcType: u30;
    VarName: u30;
  end;

  TAbcMethodBody = class
    Method: u30;
    MaxStack: u30;
    LocalCount: u30;
    InitScopeDepth: u30;
    MaxScopeDepth: u30;
    CodeLength: u30;
    Codes: array of u8;
    ExceptionCount: u30;
    TraitCount: u30;
  private
    FExceptions: TList<TAbcException>;
    FTraits: TTraitList;
  public
    constructor Create;
    destructor Destroy; override;
    property Exceptions: TList<TAbcException> read FExceptions;
    property Traits: TTraitList read FTraits;
  end;

  TAbcMethodBodies = class(TList<TAbcMethodBody>)
  public
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure WriteToStream(ABE: TBitsEngine);
  end;

{$ENDREGION}

{$REGION 'abcFile'}

type
  TAbcFile = class
    Flags: UI32;
    Name: AnsiString;
    MinorVersion: u16;
    MajorVersion: u16;
  private
    FConstPool: TAbcConstantPool;
    FMethods: TAbcMethods;
    FMetadatas: TAbcMetadatas;
    FInstances: TAbcInstances;
    FClasses: TAbcClasses;
    FScripts: TAbcScripts;
    FMethodBodies: TAbcMethodBodies;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure LoadFromStream(AStream: TStream);
    procedure LoadFromFile(const AFileName: string);
    procedure ReadFromStream(ABE: TBitsEngine);
    procedure SaveToFile(const AFileName: string);
    procedure SaveToStream(AStream: TStream);
    procedure SaveToXml(const AFileName: string);
    procedure WriteToStream(ABE: TBitsEngine);
    property ConstPool: TAbcConstantPool read FConstPool;
    property Methods: TAbcMethods read FMethods;
    property Metadatas: TAbcMetadatas read FMetadatas;
    property Instances: TAbcInstances read FInstances;
    property Classes: TAbcClasses read FClasses;
    property Scripts: TAbcScripts read FScripts;
    property MethodBodies: TAbcMethodBodies read FMethodBodies;
  end;

{$ENDREGION}

implementation

uses
  vxWin32,
  Vin.Swf;

procedure LogPos(msg: string; pos: Int32);
begin
{$IFDEF DEBUG}
  if TConsole.InConsoleMode then
    Writeln(msg, ', P=', pos);
{$ENDIF}
end;

constructor TAbcConstantPool.Create;
begin
  FIntegers := TList<s32>.Create;
  FUIntegers := TU30List.Create;
  FDoubles := TList<d64>.Create;
  FStrings := TList<AnsiString>.Create;
  FNamespaces := TList<TAbcNamespaceInfo>.Create;
  FNamespaceSets := TList<TAbcNamespaceSetInfo>.Create;
  FMultinames := TList<TAbcMultinameObjectBase>.Create;
end;

procedure TAbcConstantPool.Clear;
begin
  FIntegers.Clear();
  FUIntegers.Clear();
  FDoubles.Clear();
  FStrings.Clear();
  FNamespaces.Clear();
  FNamespaceSets.Clear();
  FMultinames.Clear();
end;

procedure TAbcConstantPool.ReadFromStream(ABE: TBitsEngine);
var
  i, j, c, c2: UInt32;
  ns: TAbcNamespaceInfo;
  nsset: TAbcNamespaceSetInfo;
  mnobj: TAbcMultinameObjectBase;
begin
  c := ABE.ReadU30;
  Integers.Add(0);
  if c > 0 then
    for i := 1 to c - 1 do
      Integers.Add(ABE.ReadU30());

  c := ABE.ReadU30();
  UIntegers.Add(0);
  if c > 0 then
    for i := 1 to c - 1 do
      UIntegers.Add(ABE.ReadU30());

  c := ABE.ReadU30();
  Doubles.Add(0.0);
  if c > 0 then
    for i := 1 to c - 1 do
      Doubles.Add(ABE.ReadStdDouble());

  c := ABE.ReadU30();
  Strings.Add('');
  if c > 0 then
    for i := 1 to c - 1 do
      Strings.Add(ABE.ReadStringWithLength());

  c := ABE.ReadU30();
  Namespaces.Add(TAbcNamespaceInfo.Create);  // "any" namespace
  if c > 0 then
    for i := 1 to c - 1 do
    begin
      ns := TAbcNamespaceInfo.Create;
      ns.ReadFromStream(ABE);
      Namespaces.Add(ns);
    end;

  c := ABE.ReadU30();
  NamespaceSets.Add(nil);
  if c > 0 then
  begin
    for i := 1 to c - 1 do
    begin
      nsset := TAbcNamespaceSetInfo.Create;
      c2 := ABE.ReadU30();   // count for ns set
      if c2 > 0 then
        for j := 1 to c2 do
          nsset.Add(ABE.ReadU30());

      NamespaceSets.Add(nsset);
    end;
  end;

  c := ABE.ReadU30();
  Multinames.Add(nil);  // entry "0" is not present in abcFile
  if c > 0 then
    for i := 1 to c - 1 do
    begin
      j := ABE.ReadU8();
      case j of
        CONSTANT_QName, CONSTANT_QNameA:            mnobj := TAbcMN_QName.Create(j);
        CONSTANT_RTQName, CONSTANT_RTQNameA:        mnobj := TAbcMN_RTQName.Create(j);
        CONSTANT_RTQNameL, CONSTANT_RTQNameLA:      mnobj := TAbcMN_RTQNameL.Create(j);
        CONSTANT_Multiname, CONSTANT_MultinameA:    mnobj := TAbcMN_Multiname.Create(j);
        CONSTANT_MultinameL, CONSTANT_MultinameLA:  mnobj := TAbcMN_MultinameL.Create(j);
        CONSTANT_GenericName:                       mnobj := TAbcMN_GenericName.Create(j);
      else
        {CONSTANT_GenericName:}                       mnobj := TAbcMN_GenericName.Create(j);
      end;
      mnobj.ReadFromStream(ABE);
      Multinames.Add(mnobj);
    end;
end;

procedure TAbcConstantPool.SaveToXml(AParentNode: IXMLDOMNode);
var
  i, j: Int32;
  node, node1, node2, node3: IXMLDOMNode;
  function GetString(id: Int32): string;
  var
    a: Int32;
  begin
    Result := Utf8ToAnsi(FStrings[id]);
    for a := 1 to Length(Result) do
    begin
      if Result[a] < ' ' then
        Result[a] := ' ';
    end;
  end;
begin
  node := AParentNode.ownerDocument.createElement('constantPool');
  AParentNode.appendChild(node);
  node1 := node.ownerDocument.createElement('integers');
  node.appendChild(node1);
  i := FIntegers.Count;
  if i = 1 then   // 只有一个 0
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FIntegers.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('integer');
      node1.appendChild(node2);
      node2.text := IntToStr(FIntegers[i]);
    end;
  end;

  node1 := node.ownerDocument.createElement('uintegers');
  node.appendChild(node1);
  i := FUIntegers.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FUIntegers.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('uinteger');
      node1.appendChild(node2);
      node2.text := IntToStr(FUIntegers[i]);
    end;
  end;

  node1 := node.ownerDocument.createElement('doubles');
  node.appendChild(node1);
  i := FDoubles.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FDoubles.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('double');
      node1.appendChild(node2);
      node2.text := FloatToStr(FDoubles[i]);
    end;
  end;

  node1 := node.ownerDocument.createElement('strings');
  node.appendChild(node1);
  i := FStrings.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FStrings.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('string');
      node1.appendChild(node2);
      node2.text := GetString(i);
    end;
  end;

  node1 := node.ownerDocument.createElement('namespaces');
  node.appendChild(node1);
  i := FNamespaces.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FNamespaces.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('namespace');
      node1.appendChild(node2);
      AddAttrInt(node2, 'kind', FNamespaces[i].Kind);
      AddAttrInt(node2, 'name', FNamespaces[i].Name);
    end;
  end;

  node1 := node.ownerDocument.createElement('namespacesets');
  node.appendChild(node1);
  i := FNamespaceSets.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FNamespaceSets.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('nsset');
      node1.appendChild(node2);
      AddAttrInt(node2, 'count', FNamespaceSets[i].Count);
      for j := 0 to FNamespaceSets[i].Count-1 do
      begin
        node3 := node2.ownerDocument.createElement('ns');
        node2.appendChild(node3);
        node3.text := IntToStr(FNamespaceSets[i][j]);
      end;
    end;
  end;

  node1 := node.ownerDocument.createElement('multinames');
  node.appendChild(node1);
  i := FMultinames.Count;
  if i = 1 then
    AddAttrInt(node1, 'count', 0)
  else
  begin
    AddAttrInt(node1, 'count', i);
    for i := 1 to FMultinames.Count-1 do
    begin
      node2 := node1.ownerDocument.createElement('multiname');
      node1.appendChild(node2);
      AddAttrInt(node2, 'kind', FMultinames[i].FKind);
    end;
  end;

end;

procedure TAbcConstantPool.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
begin
  LogPos('Integers', ABE.BitsStream.Position);
  i := FIntegers.Count;
  if i = 1 then   // 只有一个 0
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FIntegers.Count-1 do
      ABE.WriteU30(FIntegers[i]);
  end;

  LogPos('UIntegers', ABE.BitsStream.Position);
  i := FUIntegers.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FUIntegers.Count-1 do
      ABE.WriteU30(FUIntegers[i]);
  end;

  LogPos('Doubles', ABE.BitsStream.Position);
  i := FDoubles.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FDoubles.Count-1 do
      ABE.WriteStdDouble(FDoubles[i]);
  end;

  LogPos('Strings', ABE.BitsStream.Position);
  i := FStrings.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FStrings.Count-1 do
    begin
      ABE.WriteU30(Length(FStrings[i]));
      ABE.WriteString(FStrings[i], False);
    end;
  end;

  LogPos('Namespaces', ABE.BitsStream.Position);
  i := FNamespaces.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FNamespaces.Count-1 do
      FNamespaces[i].WriteToStream(ABE);
  end;

  LogPos('Namespace Sets', ABE.BitsStream.Position);
  i := FNamespaceSets.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FNamespaceSets.Count-1 do
    begin
      ABE.WriteU30(FNamespaceSets[i].Count);
      for j := 0 to FNamespaceSets[i].Count-1 do
        ABE.WriteU30(FNamespaceSets[i][j]);
    end;
  end;

  LogPos('Multinames', ABE.BitsStream.Position);
  i := FMultinames.Count;
  if i = 1 then
    ABE.WriteU30(0)
  else
  begin
    ABE.WriteU30(i);
    for i := 1 to FMultinames.Count-1 do
      FMultinames[i].WriteToStream(ABE);
  end;
end;

constructor TAbcFile.Create;
begin
  FConstPool := TAbcConstantPool.Create;
  FMethods := TAbcMethods.Create;
  FMetadatas := TAbcMetadatas.Create;
  FInstances := TAbcInstances.Create;
  FClasses := TAbcClasses.Create;
  FScripts := TAbcScripts.Create;
  FMethodBodies := TAbcMethodBodies.Create;
end;

destructor TAbcFile.Destroy;
begin
  FConstPool.Free;
  FMethods.Free;
  FMetadatas.Free;
  FInstances.Free;
  FClasses.Free;
  FScripts.Free;
  FMethodBodies.Free;

  inherited;
end;

procedure TAbcFile.Clear;
begin
  FConstPool.Clear;
  FMethods.Clear;
  FMetadatas.Clear;
  FInstances.Clear;
  FClasses.Clear;
  FScripts.Clear;
  FMethodBodies.Clear;
end;

procedure TAbcFile.LoadFromFile(const AFileName: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(fs);
  finally
    fs.Free;
  end;
end;

procedure TAbcFile.LoadFromStream(AStream: TStream);
var
  ms: TMemoryStream;
  be: TBitsEngine;
begin
  ms := TMemoryStream.Create;
  be := TBitsEngine.Create(ms);
  try
    ms.CopyFrom(AStream, AStream.Size - AStream.Position);
    ms.Position := 0;
    ReadFromStream(be);
  finally
    ms.Free;
    be.Free;
  end;
end;

procedure TAbcFile.ReadFromStream(ABE: TBitsEngine);
var
  ClassesCount: u30;
begin
  Clear();
  Flags := ABE.ReadU8;
  ABE.ReadU8;
  ABE.ReadU16;
  Name := ABE.ReadString;
  MinorVersion := ABE.ReadU16();
  MajorVersion := ABE.ReadU16();
  ConstPool.ReadFromStream(ABE);
  FMethods.ReadFromStream(ABE);
  FMetaDatas.ReadFromStream(ABE);
  ClassesCount := ABE.ReadU30;
  FInstances.ReadFromStream(ABE, ClassesCount);
  FClasses.ReadFromStream(ABE, ClassesCount);
  FScripts.ReadFromStream(ABE);
  FMethodBodies.ReadFromStream(ABE);
end;

procedure TAbcFile.SaveToFile(const AFileName: string);
var
  fs: TFileStream;
  be: TBitsEngine;
begin
  fs := TFileStream.Create(AFileName, fmCreate);
  be := TBitsEngine.Create(fs);
  try
    WriteToStream(be);
  finally
    be.Free;
    fs.Free;
  end;
end;

procedure TAbcFile.SaveToStream(AStream: TStream);
var
  be: TBitsEngine;
begin
  AStream.Size := 0;
  be := TBitsEngine.Create(AStream);
  try
    WriteToStream(be);
  finally
    be.Free;
  end;
end;

procedure TAbcFile.SaveToXml(const AFileName: string);
var
  xml: IXMLDOMDocument;
//  node: IXMLDOMElement;
begin
  xml := CoDOMDocument.Create;
  xml.documentElement := xml.createElement('abc');
  AddAttrInt(xml.documentElement, 'flags', Flags);
  AddAttr(xml.documentElement, 'name', string(Name));
  AddAttrInt(xml.documentElement, 'minorVer', MinorVersion);
  AddAttrInt(xml.documentElement, 'majorVer', MajorVersion);

  FConstPool.SaveToXml(xml.documentElement);
  FMethods.SaveToXml(xml.documentElement);

  xml.save(AFileName);
end;

procedure TAbcFile.WriteToStream(ABE: TBitsEngine);
begin
  ABE.WriteU32(Flags);
  if Boolean(Length(Name)) then
    ABE.WriteBuffer(Name[1], Length(Name));
  ABE.WriteU8(0);

  ABE.WriteU16(MinorVersion);
  ABE.WriteU16(MajorVersion);
  FConstPool.WriteToStream(ABE);
  FMethods.WriteToStream(ABE);
  FMetadatas.WriteToStream(ABE);
  ABE.WriteU30(FInstances.Count);
  FInstances.WriteToStream(ABE);
  FClasses.WriteToStream(ABE);
  FScripts.WriteToStream(ABE);
  FMethodBodies.WriteToStream(ABE);
end;

constructor TAbcMethodInfo.Create;
begin
  FParamTypes := TU30List.Create;
  FOptionals := TAbcOptionals.Create;
  FParamNames := TU30List.Create;
end;

function TAbcMethodInfo.GetParamCount: u30;
begin
  Result := FParamTypes.Count;
end;

{ TAbcMethodInfo }

procedure TAbcMethodInfo.ReadFromStream(ABE: TBitsEngine);
var
  optDetail: TAbcOptionalDetail;
  i, u, LParamCount: Int32;
begin
  LParamCount := ABE.ReadU30();
  FReturnType := ABE.ReadU30();
  if LParamCount > 0 then
    for i := 1 to LParamCount do
      FParamTypes.Add(ABE.ReadU30());

  FName := ABE.ReadU30();
  FFlags := ABE.ReadU8();

  if HasValue(FFlags, METHOD_HAS_OPTIONAL) then
  begin
    u := ABE.ReadU30();
    if u > 0 then
      for i := FParamTypes.Count - u to FParamTypes.Count-1 do
      begin
        optDetail := TAbcOptionalDetail.Create;
        optDetail.Val := ABE.ReadU30();
        optDetail.Kind := ABE.ReadU8();
        FOptionals.Add(optDetail);
      end;
  end;

  if HasValue(FFlags, METHOD_HAS_PARAM_NAMES) then
  begin
    if LParamCount > 0 then
      for i := 1 to LParamCount do
        ParamNames.Add(ABE.ReadU30());
  end;
end;

procedure TAbcMethodInfo.WriteToStream(ABE: TBitsEngine);
var
  optDetail: TAbcOptionalDetail;
  j: UInt32;
begin
    ABE.WriteU30(ParamCount);
    ABE.WriteU30(FReturnType);
    if ParamCount > 0 then
      for j := 0 to ParamCount-1 do
        ABE.WriteU30(FParamTypes[j]);
    ABE.WriteU30(FName);
    ABE.WriteU8(FFlags);

    if HasValue(FFlags, METHOD_HAS_OPTIONAL) then
    begin
      ABE.WriteU30(FOptionals.Count);
      if FOptionals.Count > 0 then
        for j := 0 to FOptionals.Count-1 do
        begin
          optDetail := FOptionals[j];
          ABE.WriteU30(optDetail.Val);
          ABE.WriteU8(optDetail.Kind);
        end;
    end;

    if HasValue(FFlags, METHOD_HAS_PARAM_NAMES) and (ParamCount > 0)then
      for j := 0 to ParamCount-1 do
        ABE.WriteU30(FParamNames[j]);
end;

constructor TAbcMetadata.Create;
begin
  FItems := TList<TAbcMetaitem>.Create;
end;

constructor TAbcTrait.Create;
begin
  FMetadatas := TU30List.Create;
  FData := nil;
end;

function TAbcTrait.GetKindAndAttr: u8;
begin
  FKindAndAttr := FAttr shl 4 + FKind;
  Result := FKindAndAttr;
end;

procedure TAbcTrait.SetKindAndAttr(const Value: u8);
begin
  FKindAndAttr := Value;
  FAttr := FKindAndAttr shr 4;
  FKind := FKindAndAttr and $F;
end;

procedure TAbcTrait.ReadFromStream(ABE: TBitsEngine);
var
  i, c: Int32;
  trSlot: TAbcTraitSlot;
  trClass: TAbcTraitClass;
  trFunc: TAbcTraitFunction;
  trMethod: TAbcTraitMethod;
begin
  Name := ABE.ReadU30();
  KindAndAttr := ABE.ReadU8();

  case Kind of
    CONSTANT_Trait_Slot, CONSTANT_Trait_Const:
      begin
        trSlot := TAbcTraitSlot.Create;
        trSlot.SlotId := ABE.ReadU30();
        trSlot.TypeName := ABE.ReadU30();
        trSlot.vindex := ABE.ReadU30();
        if trSlot.vindex > 0 then
          trSlot.vkind := ABE.ReadU8();

        Data := trSlot;
      end;
    CONSTANT_Trait_Class:
      begin
        trClass := TAbcTraitClass.Create;
        trClass.SlotId := ABE.ReadU30();
        trClass.Classi := ABE.ReadU30();

        Data := trClass;
      end;
    CONSTANT_Trait_Function:
      begin
        trFunc := TAbcTraitFunction.Create;
        trFunc.SlotId := ABE.ReadU30();;
        trFunc.Function_ := ABE.ReadU30();

        Data := trFunc;
      end;
    CONSTANT_Trait_Method, CONSTANT_Trait_Getter, CONSTANT_Trait_Setter:
      begin
        trMethod := TAbcTraitMethod.Create;
        trMethod.DispId := ABE.ReadU30();
        trMethod.Method := ABE.ReadU30();

        Data := trMethod;
      end;
  end;

  if HasValue(Attr, CONSTANT_Trait_ATTR_Metadata) then
  begin
    c := ABE.ReadU30();
    if c > 0 then
      for i := 1 to c do
        Metadatas.Add(ABE.ReadU30());
  end;
end;

procedure TAbcTrait.WriteToStream(ABE: TBitsEngine);
var
  i: Int32;
  trSlot: TAbcTraitSlot;
  trClass: TAbcTraitClass;
  trFunc: TAbcTraitFunction;
  trMethod: TAbcTraitMethod;
begin
  ABE.WriteU30(FName);
  ABE.WriteU8(KindAndAttr);

  case Kind of
    CONSTANT_Trait_Slot, CONSTANT_Trait_Const:
      begin
        trSlot := TAbcTraitSlot(FData);
        ABE.WriteU30(trSlot.SlotId);
        ABE.WriteU30(trSlot.TypeName);
        ABE.WriteU30(trSlot.vindex);
        if trSlot.vindex > 0 then
          ABE.WriteU8(trSlot.vkind);
      end;
    CONSTANT_Trait_Class:
      begin
        trClass := TAbcTraitClass(FData);
        ABE.WriteU30(trClass.SlotId);
        ABE.WriteU30(trClass.Classi);
      end;
    CONSTANT_Trait_Function:
      begin
        trFunc := TAbcTraitFunction(FData);
        ABE.WriteU30(trFunc.SlotId);
        ABE.WriteU30(trFunc.Function_);
      end;
    CONSTANT_Trait_Method, CONSTANT_Trait_Getter, CONSTANT_Trait_Setter:
      begin
        trMethod := TAbcTraitMethod(FData);
        ABE.WriteU30(trMethod.DispId);
        ABE.WriteU30(trMethod.Method);
      end;
  end;

  if HasValue(Attr, CONSTANT_Trait_ATTR_Metadata) then
  begin
    ABE.WriteU30(Metadatas.Count);
      for i := 0 to Metadatas.Count-1 do
        ABE.WriteU30(Metadatas[i]);
  end;
end;

{ TAbcInstance }

constructor TAbcInstance.Create;
begin
  FInterfaces := TU30List.Create;
  FTraits := TTraitList.Create;
end;

destructor TAbcInstance.Destroy;
begin
  FInterfaces.Free;
  FTraits.Free;
end;

{ TAbcClassInfo }

constructor TAbcClassInfo.Create;
begin
  FTraits := TTraitList.Create;
end;

destructor TAbcClassInfo.Destroy;
begin
  Traits.Free;
end;

{ TAbcScript }

constructor TAbcScript.Create;
begin
  FTraits := TTraitList.Create;
end;

destructor TAbcScript.Destroy;
begin
  FTraits.Free;
end;

{ TAbcMethodBody }

constructor TAbcMethodBody.Create;
begin
  SetLength(Codes, 0);
  FExceptions := TList<TAbcException>.Create;
  FTraits := TTraitList.Create;
end;

destructor TAbcMethodBody.Destroy;
begin
  SetLength(Codes, 0);
  FExceptions.Free;
  FTraits.Free;
end;

{ TAbcNamespaceInfo }

constructor TAbcNamespaceInfo.Create;
begin
  FKind := 0;
  FName := 0;
end;

procedure TAbcNamespaceInfo.ReadFromStream(ABE: TBitsEngine);
begin
  FKind := ABE.ReadU8();
  FName := ABE.ReadU30();
end;

procedure TAbcNamespaceInfo.WriteToStream(ABE: TBitsEngine);
begin
  ABE.WriteU8(FKind);
  ABE.WriteU30(FName);
end;

{ TAbcMultinameObjectBase }

constructor TAbcMultinameObjectBase.Create(AKind: u8);
begin
  FKind := AKind;
end;

procedure TAbcMultinameObjectBase.ReadFromStream(ABE: TBitsEngine);
begin

end;

procedure TAbcMultinameObjectBase.WriteToStream(ABE: TBitsEngine);
begin
  ABE.WriteU8(FKind);
end;

{ TAbcMN_QName }

procedure TAbcMN_QName.ReadFromStream(ABE: TBitsEngine);
begin
  FNS := ABE.ReadU30;
  FName := ABE.ReadU30;
end;

procedure TAbcMN_QName.WriteToStream(ABE: TBitsEngine);
begin
  inherited;
  ABE.WriteU30(FNS);
  ABE.WriteU30(FName);
end;

{ TAbcMN_RTQName }

procedure TAbcMN_RTQName.ReadFromStream(ABE: TBitsEngine);
begin
  FName := ABE.ReadU30;
end;

procedure TAbcMN_RTQName.WriteToStream(ABE: TBitsEngine);
begin
  inherited;
  ABE.WriteU30(FName);
end;

{ TAbcMN_Multiname }

procedure TAbcMN_Multiname.ReadFromStream(ABE: TBitsEngine);
begin
  FName := ABE.ReadU30;
  FNsSet := ABE.ReadU30;
end;

procedure TAbcMN_Multiname.WriteToStream(ABE: TBitsEngine);
begin
  inherited;
  ABE.WriteU30(FName);
  ABE.WriteU30(FNsSet);
end;

{ TAbcMN_MultinameL }

procedure TAbcMN_MultinameL.ReadFromStream(ABE: TBitsEngine);
begin
  FNsSet := ABE.ReadU30;
end;

procedure TAbcMN_MultinameL.WriteToStream(ABE: TBitsEngine);
begin
  inherited;
  ABE.WriteU30(FNsSet);
end;

{ TAbcMN_GenericName }

constructor TAbcMN_GenericName.Create(AKind: u8);
begin
  inherited Create(AKind);
  FParams := TU30List.Create;
end;

procedure TAbcMN_GenericName.ReadFromStream(ABE: TBitsEngine);
var
  i, c: Int32;
begin
  FTypeDefinition := ABE.ReadU30;
  c := ABE.ReadU8;
  for i := 1 to c do
    FParams.Add(ABE.ReadU30);
end;

procedure TAbcMN_GenericName.WriteToStream(ABE: TBitsEngine);
var
  i: Int32;
begin
  inherited;
  ABE.WriteU30(FTypeDefinition);
  ABE.WriteU30(FParams.Count);
  for i := 0 to FParams.Count-1 do
    ABE.WriteU30(FParams[i]);
end;

{ TAbcMethods }

procedure TAbcMethods.ReadFromStream(ABE: TBitsEngine);
var
  mi: TAbcMethodInfo;
  i, c: Int32;
begin
  c := ABE.ReadU30();
  if c > 0 then
  begin
    for i := 1 to c do
    begin
      mi := TAbcMethodInfo.Create;
      mi.ReadFromStream(ABE);
      Self.Add(mi);
    end;
  end;
end;

procedure TAbcMethods.SaveToXml(AParentNode: IXMLDOMNode);
begin

end;

procedure TAbcMethods.WriteToStream(ABE: TBitsEngine);
var
  mi: TAbcMethodInfo;
  i: UInt32;
begin
  ABE.WriteU30(Count);
  for i := 0 to Count-1 do
  begin
    mi := Items[i];
    mi.WriteToStream(ABE);
  end;
end;

{ TAbcMetadatas }

procedure TAbcMetadatas.ReadFromStream(ABE: TBitsEngine);
var
  i, j, c, c1: Int32;
  meta: TAbcMetadata;
  item: TAbcMetaitem;
begin
  c := ABE.ReadU30();
  if c > 0 then
  begin
    for i := 1 to c do
    begin
      meta := TAbcMetadata.Create;
      meta.Name := ABE.ReadU30();
      c1 := ABE.ReadU30();
      if c1 > 0 then
        for j := 1 to c1 do
        begin
          item := TAbcMetaitem.Create;
          item.Key := ABE.ReadU30();
          item.Value := ABE.ReadU30();
          meta.Items.Add(item);
        end;

      Self.Add(meta);
    end;
  end;
end;

procedure TAbcMetadatas.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
  meta: TAbcMetadata;
  item: TAbcMetaitem;
begin
  ABE.WriteU30(Count);
  for i := 0 to Count-1 do
  begin
    meta := Items[i];
    ABE.WriteU30(meta.Name);
    ABE.WriteU30(meta.Items.Count);
    for j := 0 to meta.Items.Count-1 do
    begin
      item := meta.Items[j];
      ABE.WriteU30(item.Key);
      ABE.WriteU30(item.Value);
    end;
  end;
end;

{ TAbcInstances }

procedure TAbcInstances.ReadFromStream(ABE: TBitsEngine; AInstCount: Int32);
var
  i, j, c: Int32;
  inst: TAbcInstance;
  trait: TAbcTrait;
begin
  if AInstCount > 0 then
  begin
    for i := 1 to AInstCount do
    begin
      inst := TAbcInstance.Create;
      inst.Name := ABE.ReadU30();
      inst.SuperName := ABE.ReadU30();
      inst.Flags := ABE.ReadU8();
      if HasValue(inst.Flags, CONSTANT_ClassProtectedNs) then
        inst.protectedNs := ABE.ReadU30();
      c := ABE.ReadU30();
      if c > 0 then
        for j := 1 to c do
          inst.interfaces.Add(ABE.ReadU30());

      inst.iinit := ABE.ReadU30();
      c := ABE.ReadU30();
      if c > 0 then
        for j := 1 to c do
        begin
          trait := TAbcTrait.Create;
          trait.ReadFromStream(ABE);
          inst.traits.Add(trait);
        end;

      Self.Add(inst);
    end;
  end;
end;

procedure TAbcInstances.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
  inst: TAbcInstance;
  trait: TAbcTrait;
begin
  for i := 0 to Count-1 do
  begin
    inst := Items[i];
    ABE.WriteU30(inst.Name);
    ABE.WriteU30(inst.SuperName);
    ABE.WriteU8(inst.Flags);
    if HasValue(inst.Flags, CONSTANT_ClassProtectedNs) then
      ABE.WriteU30(inst.ProtectedNs);
    ABE.WriteU30(inst.Interfaces.Count);
    for j := 0 to inst.Interfaces.Count-1 do
      ABE.WriteU30(inst.Interfaces[j]);
    ABE.WriteU30(inst.IInit);
    ABE.WriteU30(inst.Traits.Count);
    for j := 0 to inst.Traits.Count-1 do
    begin
      trait := inst.Traits[j];
      trait.WriteToStream(ABE);
    end;
  end;
end;

{ TAbcClasses }

procedure TAbcClasses.ReadFromStream(ABE: TBitsEngine; ACount: Int32);
var
  i, j, c: Int32;
  ci: TAbcClassInfo;
  tr: TAbcTrait;
begin
  if ACount > 0 then
    for i := 1 to ACount do
    begin
      ci := TAbcClassInfo.Create;
      ci.cinit := ABE.ReadU30();
      c := ABE.ReadU30();
      if c > 0 then
        for j := 1 to c do
        begin
          tr := TAbcTrait.Create;
          tr.ReadFromStream(ABE);
          ci.traits.Add(tr);
        end;

      Self.Add(ci);
    end;
end;

procedure TAbcClasses.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
  ci: TAbcClassInfo;
  tr: TAbcTrait;
begin
  for i := 0 to Count-1 do
  begin
    ci := Items[i];
    ABE.WriteU30(ci.CInit);
    ABE.WriteU30(ci.Traits.Count);
    for j := 0 to ci.Traits.Count-1 do
    begin
      tr := ci.Traits[j];
      tr.WriteToStream(ABE);
    end;
  end;
end;

procedure TAbcScripts.ReadFromStream(ABE: TBitsEngine);
var
  i, j, c, c1: Int32;
  script: TAbcScript;
  tr: TAbcTrait;
begin
  c := ABE.ReadU30();
  if c > 0 then
    for i := 1 to c do
    begin
      script := TAbcScript.Create;
      script.Init := ABE.ReadU30();
      c1 := ABE.ReadU30();
      if c1 > 0 then
        for j := 1 to c1 do
        begin
          tr := TAbcTrait.Create;
          tr.ReadFromStream(ABE);
          script.Traits.Add(tr);
        end;

      Self.Add(script);
    end;
end;

procedure TAbcScripts.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
  script: TAbcScript;
  tr: TAbcTrait;
begin
  ABE.WriteU30(Count);
  for i := 0 to Count-1 do
  begin
    script := Items[i];
    ABE.WriteU30(script.Init);
    ABE.WriteU30(script.Traits.Count);
    for j := 0 to script.Traits.Count-1 do
    begin
      tr := script.Traits[j];
      tr.WriteToStream(ABE);
    end;
  end;
end;

procedure TAbcMethodBodies.ReadFromStream(ABE: TBitsEngine);
var
  i, j, c: Int32;
  body: TAbcMethodBody;
  exc: TAbcException;
  tr: TAbcTrait;
begin
  c := ABE.ReadU30();
  if c > 0 then
    for i := 1 to c do
    begin
      body := TAbcMethodBody.Create;
      body.Method := ABE.ReadU30();
      body.MaxStack := ABE.ReadU30();
      body.LocalCount := ABE.ReadU30();
      body.InitScopeDepth := ABE.ReadU30();
      body.MaxScopeDepth := ABE.ReadU30();
      body.CodeLength := ABE.ReadU30();
      SetLength(body.Codes, body.CodeLength);
      ABE.ReadBuffer(body.Codes[0], body.CodeLength);

      body.ExceptionCount := ABE.ReadU30();
      if body.ExceptionCount > 0 then
        for j := 1 to body.ExceptionCount do
        begin
          exc := TAbcException.Create;
          exc.From := ABE.ReadU30();
          exc.To_ := ABE.ReadU30();
          exc.Target := ABE.ReadU30();
          exc.ExcType := ABE.ReadU30();
          exc.VarName := ABE.ReadU30();

          body.Exceptions.Add(exc);
        end;

      body.TraitCount := ABE.ReadU30();
      if body.TraitCount > 0 then
        for j := 1 to body.TraitCount do
        begin
          tr := TAbcTrait.Create;
          tr.ReadFromStream(ABE);
          body.Traits.Add(tr);
        end;

      Self.Add(body);
    end;
end;

procedure TAbcMethodBodies.WriteToStream(ABE: TBitsEngine);
var
  i, j: Int32;
  body: TAbcMethodBody;
  exc: TAbcException;
  tr: TAbcTrait;
begin
  ABE.WriteU30(Count);
  for i := 0 to Count-1 do
  begin
    body := Items[i];
    ABE.WriteU30(body.Method);
    ABE.WriteU30(body.MaxStack);
    ABE.WriteU30(body.LocalCount);
    ABE.WriteU30(body.InitScopeDepth);
    ABE.WriteU30(body.MaxScopeDepth);
    ABE.WriteU30(body.CodeLength);
    ABE.WriteBuffer(body.Codes[0], body.CodeLength);

    ABE.WriteU30(body.Exceptions.Count);
    for j := 0 to body.Exceptions.Count-1 do
    begin
      exc := body.Exceptions[j];
      ABE.WriteU30(exc.From);
      ABE.WriteU30(exc.To_);
      ABE.WriteU30(exc.Target);
      ABE.WriteU30(exc.ExcType);
      ABE.WriteU30(exc.VarName);
    end;

    ABE.WriteU30(body.Traits.Count);
    for j := 0 to body.Traits.Count-1 do
    begin
      tr := body.Traits[j];
      tr.WriteToStream(ABE);
    end;
  end;
end;

initialization
  CoInitialize(nil);

end.

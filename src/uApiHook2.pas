unit uApiHook2;

interface

uses
  Windows, SysUtils, Classes, Generics.Collections, WinInet, UrlMon, TlHelp32, Math, SyncObjs, RegularExpressions,
  Vin.Win32.ApiHook_LDE32, Vin.Win32.LDE32, uSwfHacker, vxUtils;

type
  TInternetConnectA = function(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT;
    lpszUsername: PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD)
    : HINTERNET; stdcall;

  THttpOpenRequestA = function(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar;
    lpszVersion: PAnsiChar; lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD)
    : HINTERNET; stdcall;

  THttpAddRequestHeadersA = function(hRequest: HINTERNET; lpszHeaders: PAnsiChar; dwHeadersLength: DWORD;
    dwModifiers: DWORD): BOOL; stdcall;

  THttpQueryInfoA = function(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
    var lpdwReserved: DWORD): BOOL; stdcall;

  TInternetQueryDataAvailable = function(hFile: HINTERNET; var lpdwNumberOfBytesAvailable: DWORD;
    dwFlags, dwContext: DWORD): BOOL; stdcall;

  TInternetReadFile = function(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD;
    var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;

  TInternetCloseHandle = function(hInet: HINTERNET): BOOL; stdcall;

{$REGION '钩子管理'}
procedure HookApi2;

procedure UnhookApi2;
{$ENDREGION}
{
function MyHttpAddRequestHeadersA(hRequest: HINTERNET; lpszHeaders: PAnsiChar; dwHeadersLength: DWORD; dwModifiers:
    DWORD): BOOL; stdcall;

function MyHttpOpenRequestA(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar; lpszVersion:
    PAnsiChar; lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD): HINTERNET;
    stdcall;

function MyHttpQueryInfoA(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
    var lpdwReserved: DWORD): BOOL; stdcall;

function MyInternetCloseHandle(hInet: HINTERNET): BOOL; stdcall;

function MyInternetConnectA(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT; lpszUsername:
    PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD): HINTERNET; stdcall;

function MyInternetQueryDataAvailable(hFile: HINTERNET; var lpdwNumberOfBytesAvailable: DWORD;
  dwFlags, dwContext: DWORD): BOOL; stdcall;

function MyInternetReadFile(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD; var
    lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;
}
function ReplaceHttpHeader(AInputHeader, AParamName, AValue: string): string; inline;

implementation

var
  hkInternetConnectA: TNtApiHookLDE32<TInternetConnectA> = nil;
  hkHttpOpenRequestA: TNtApiHookLDE32<THttpOpenRequestA> = nil;
  hkHttpAddRequestHeadersA: TNtApiHookLDE32<THttpAddRequestHeadersA> = nil;
  hkHttpQueryInfoA: TNtApiHookLDE32<THttpQueryInfoA> = nil;
  hkInternetQueryDataAvailable: TNtApiHookLDE32<TInternetQueryDataAvailable> = nil;
  hkInternetReadFile: TNtApiHookLDE32<TInternetReadFile> = nil;
  hkInternetCloseHandle: TNtApiHookLDE32<TInternetCloseHandle> = nil;
  
  INetDownloadLock: TRTLCriticalSection;
  INetDownloading: Boolean = false;
  HostNameDict: TDictionary<UInt32, string> = nil;      // <hConnect, HostName>
  UrlDict: TDictionary<UInt32, string> = nil;           // <hRequest, Url>
  FileStreamDict: TObjectDictionary<UInt32, TMemoryStream> = nil;   // <hRequest, File Stream>

function INetGetFile(fileURL: string; ms: TMemoryStream): Boolean;
const
  UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727)';
var
  hSession, hURL: HINTERNET;
  Buffer: array [0 .. 1023] of BYTE;
  BufferLen: DWORD;
begin
  Result := False;
  hSession := InternetOpen(PChar(UserAgent), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0);
    repeat
      if InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) then
        ms.Write(Buffer[0], BufferLen);
    until BufferLen = 0;
    Result := True;
    InternetCloseHandle(hURL);
  finally
    InternetCloseHandle(hSession);
  end;
end;

function MyHttpAddRequestHeadersA(hRequest: HINTERNET; lpszHeaders: PAnsiChar; dwHeadersLength: DWORD;
  dwModifiers: DWORD): BOOL; stdcall;
var
  req: AnsiString;
begin
  if INetDownloading then
  begin
    Result := hkHttpAddRequestHeadersA.BaseFunc(hRequest, lpszHeaders, dwHeadersLength, dwModifiers);
    Exit;
  end;
  
  req := AnsiString(lpszHeaders);
  if Pos('Accept-Encoding', req) > 0 then
  begin
    if req[Length(req)] <> #$D then
    begin
      SetLength(req, Length(req) + 1);
      req[Length(req)] := #$D;
    end;
    req := AnsiString(ReplaceHttpHeader(string(req), 'Accept-Encoding', 'identity'));
    Delete(req, Length(req), 1);
    Result := hkHttpAddRequestHeadersA.BaseFunc(hRequest, PAnsiChar(req), Length(req), dwModifiers or HTTP_ADDREQ_FLAG_ADD or HTTP_ADDREQ_FLAG_REPLACE);
  end
  else
    Result := hkHttpAddRequestHeadersA.BaseFunc(hRequest, lpszHeaders, dwHeadersLength, dwModifiers);
end;

function MyHttpOpenRequestA(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar; lpszVersion: PAnsiChar;
  lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD): HINTERNET; stdcall;
var
  host, url: string;
  ms: TMemoryStream;
begin
  if INetDownloading then
  begin
    Result := hkHttpOpenRequestA.BaseFunc(hConnect, lpszVerb, lpszObjectName, lpszVersion, lpszReferrer, 
                                          lplpszAcceptTypes, dwFlags, dwContext);
    Exit;
  end;

  Result := hkHttpOpenRequestA.BaseFunc(hConnect, lpszVerb, lpszObjectName, lpszVersion, lpszReferrer, lplpszAcceptTypes,
      dwFlags { or INTERNET_FLAG_NO_CACHE_WRITE  or INTERNET_FLAG_RELOAD}, dwContext);

  if HostNameDict.ContainsKey(UInt32(hConnect)) then
  begin
    host := HostNameDict[UInt32(hConnect)];
    url := 'http://' + host + string(AnsiString(lpszObjectName));

    INetDownloadLock.Enter;
    try
      if NeedConvert(url) then
      begin
        UrlDict.AddOrSetValue(UInt32(Result), url);
        ms := TMemoryStream.Create;
        if Length(CurMapItem.local) > 0 then    // 使用本地文件
          TryGetLocalFile(ms)                   // todo: 如果加载失败怎么办？
        else
        begin
          try
          {
            hkInternetConnectA.Unhook;
            hkHttpOpenRequestA.Unhook;
            //hkHttpAddRequestHeadersA.Unhook;
            hkHttpQueryInfoA.Unhook;
            hkInternetQueryDataAvailable.Unhook;
            hkInternetReadFile.Unhook;
          }
            INetDownloading := True;
            INetGetFile(url, ms);
            INetDownloading := False;
          finally
          {
            hkInternetConnectA.Hook;
            hkHttpOpenRequestA.Hook;
            //hkHttpAddRequestHeadersA.Hook;
            hkHttpQueryInfoA.Hook;
            hkInternetQueryDataAvailable.Hook;
            hkInternetReadFile.Hook;
          }
          end;
        end;
        ConvertSwf(ms);
        ms.Position := 0;
        FileStreamDict.AddOrSetValue(UInt32(Result), ms);
      end;
    finally
      INetDownloadLock.Leave;
    end;
  end;
end;

function MyHttpQueryInfoA(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
  var lpdwReserved: DWORD): BOOL; stdcall;
var
  s2: AnsiString;
  s: string;
  LBufLen, LOldBufLen, LInfoLevel, LInfoLevelHigh: DWORD;

  procedure ModifyHeader;
  begin
    s := string(AnsiString(PAnsiChar(lpvBuffer)));
    s := ReplaceHttpHeader(s, 'Content-Length', IntToStr(FileStreamDict[UInt32(hRequest)].Size));
    s2 := AnsiString(s);
    LBufLen := Length(s2);
    if LBufLen > LOldBufLen then
    begin
      Result := False;
      lpdwBufferLength := LBufLen + 10;
      SetLastError(ERROR_INSUFFICIENT_BUFFER);
      Exit;
    end;

    Move(s2[1], lpvBuffer^, LBufLen);
    lpdwBufferLength := LBufLen;
  end;
begin
  LOldBufLen := lpdwBufferLength;
  Result := hkHttpQueryInfoA.BaseFunc(hRequest, dwInfoLevel, lpvBuffer, lpdwBufferLength, lpdwReserved);
  if (not Result) or INetDownloading then
    Exit;

  if UrlDict.ContainsKey(UInt32(hRequest)) and FileStreamDict.ContainsKey(UInt32(hRequest)) then
  begin
    LInfoLevel := dwInfoLevel and $0000FFFF;
    LInfoLevelHigh := dwInfoLevel and $FFFF0000;
    case LInfoLevel of
      HTTP_QUERY_CONTENT_LENGTH:
        begin
          if HasValue(LInfoLevelHigh, HTTP_QUERY_FLAG_REQUEST_HEADERS shr 4) or
             HasValue(LInfoLevelHigh, HTTP_QUERY_FLAG_REQUEST_HEADERS)
          then
            PDWORD(lpvBuffer)^ := FileStreamDict[UInt32(hRequest)].Size
          else
            ModifyHeader;
        end;
      HTTP_QUERY_RAW_HEADERS_CRLF: ModifyHeader;
    end;
  end;
end;

function MyInternetCloseHandle(hInet: HINTERNET): BOOL; stdcall;
begin
  Result := hkInternetCloseHandle.BaseFunc(hInet);
  if INetDownloading then
    Exit;
    
  HostNameDict.Remove(UInt32(hInet));
  UrlDict.Remove(UInt32(hInet));
  FileStreamDict.Remove(UInt32(hInet));
end;

function MyInternetConnectA(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT;
  lpszUsername: PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD)
  : HINTERNET; stdcall;
begin
  Result := hkInternetConnectA.BaseFunc(hInet, lpszServerName, nServerPort, lpszUsername, lpszPassword, dwService, 
                                        dwFlags, dwContext);
  if INetDownloading then
    Exit;
    
  if not HostNameDict.ContainsKey(UInt32(Result)) then
    HostNameDict.Add(UInt32(Result), string(AnsiString(lpszServerName)));
end;

function MyInternetQueryDataAvailable(hFile: HINTERNET; var lpdwNumberOfBytesAvailable: DWORD;
  dwFlags, dwContext: DWORD): BOOL; stdcall;
var
  a: UInt32;
begin
  Result := hkInternetQueryDataAvailable.BaseFunc(hFile, lpdwNumberOfBytesAvailable, dwFlags, dwContext);
  if INetDownloading then
    Exit;
  
  if UrlDict.ContainsKey(UInt32(hFile)) and FileStreamDict.ContainsKey(UInt32(hFile)) then
  begin
    with FileStreamDict[UInt32(hFile)] do
      a := Size - Position;
    if a > lpdwNumberOfBytesAvailable then
      lpdwNumberOfBytesAvailable := 8192
    else
      lpdwNumberOfBytesAvailable := a;
  end;
end;

function MyInternetReadFile(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD;
  var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;
begin
  Result := hkInternetReadFile.BaseFunc(hFile, lpBuffer, dwNumberOfBytesToRead, lpdwNumberOfBytesRead);
  if INetDownloading then
    Exit;
    
  if UrlDict.ContainsKey(UInt32(hFile)) and FileStreamDict.ContainsKey(UInt32(hFile)) then
  begin // 判断是不是目标文件
    ZeroMemory(lpBuffer, dwNumberOfBytesToRead);
    lpdwNumberOfBytesRead := FileStreamDict[UInt32(hFile)].Read(lpBuffer^, dwNumberOfBytesToRead);
    Result := True;
  end;
end;

function ReplaceHttpHeader(AInputHeader, AParamName, AValue: string): string;
begin
  Result := TRegEx.Replace(AInputHeader, AParamName+':(.*?)\r', AParamName+': '+AValue+#$D);
end;

{$REGION '钩子管理'}
procedure HookApi2;
begin
  if hkInternetConnectA = nil then
    hkInternetConnectA := TNtApiHookLDE32<TInternetConnectA>.Create('wininet.dll', 'InternetConnectA', @MyInternetConnectA);
  hkInternetConnectA.Hook;

  if hkHttpOpenRequestA = nil then
    hkHttpOpenRequestA := TNtApiHookLDE32<THttpOpenRequestA>.Create('wininet.dll', 'HttpOpenRequestA', @MyHttpOpenRequestA);
  hkHttpOpenRequestA.Hook;

  if hkHttpAddRequestHeadersA = nil then
    hkHttpAddRequestHeadersA := TNtApiHookLDE32<THttpAddRequestHeadersA>.Create('wininet.dll', 'HttpAddRequestHeadersA', @MyHttpAddRequestHeadersA);
  hkHttpAddRequestHeadersA.Hook;

  if hkHttpQueryInfoA = nil then
    hkHttpQueryInfoA := TNtApiHookLDE32<THttpQueryInfoA>.Create('wininet.dll', 'HttpQueryInfoA', @MyHttpQueryInfoA);
  hkHttpQueryInfoA.Hook;

  if hkInternetQueryDataAvailable = nil then
    hkInternetQueryDataAvailable := TNtApiHookLDE32<TInternetQueryDataAvailable>.Create('wininet.dll', 'InternetQueryDataAvailable', @MyInternetQueryDataAvailable);
  hkInternetQueryDataAvailable.Hook;

  if hkInternetReadFile = nil then
    hkInternetReadFile := TNtApiHookLDE32<TInternetReadFile>.Create('wininet.dll', 'InternetReadFile', @MyInternetReadFile);
  hkInternetReadFile.Hook;

  if hkInternetCloseHandle = nil then
    hkInternetCloseHandle := TNtApiHookLDE32<TInternetCloseHandle>.Create('wininet.dll', 'InternetCloseHandle', @MyInternetCloseHandle);
  hkInternetCloseHandle.Hook;
end;

procedure UnhookApi2;
begin
  if Assigned(hkInternetCloseHandle) then
  begin
    hkInternetCloseHandle.Unhook;
    hkInternetCloseHandle.Free;
  end;

  if Assigned(hkInternetReadFile) then
  begin
    hkInternetReadFile.Unhook;
    hkInternetReadFile.Free;
  end;

  if Assigned(hkInternetQueryDataAvailable) then
  begin
    hkInternetQueryDataAvailable.Unhook;
    hkInternetQueryDataAvailable.Free;
  end;

  if Assigned(hkHttpQueryInfoA) then
  begin
    hkHttpQueryInfoA.Unhook;
    hkHttpQueryInfoA.Free;
  end;

  if Assigned(hkHttpAddRequestHeadersA) then
  begin
    hkHttpAddRequestHeadersA.Unhook;
    hkHttpAddRequestHeadersA.Free;
  end;

  if Assigned(hkHttpOpenRequestA) then
  begin
    hkHttpOpenRequestA.Unhook;
    hkHttpOpenRequestA.Free;
  end;

  if Assigned(hkInternetConnectA) then
  begin
    hkInternetConnectA.Unhook;
    hkInternetConnectA.Free;
  end;
end;
{$ENDREGION}

initialization
  INetDownloadLock.Initialize;
  HostNameDict := TDictionary<UInt32, string>.Create;
  UrlDict := TDictionary<UInt32, string>.Create;
  FileStreamDict := TObjectDictionary<UInt32, TMemoryStream>.Create([doOwnsValues]);

end.

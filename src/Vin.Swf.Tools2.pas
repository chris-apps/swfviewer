unit Vin.Swf.Tools2;

interface

uses
  Windows, SysUtils, Classes, Graphics,
  Vin.Swf.Types, Vin.Swf.Consts;

type
  TBitsEngine = class
  private
    FBitsStream: TStream;
    FLastBitCount: Byte;
    FLastByte: Byte;
  public
    constructor Create(bs: TStream);
    procedure FlushLastByte(write: Boolean = True);
    function GetBits(n: Integer): DWord;
    function GetSBits(n: Integer): LongInt;
    procedure ReadBuffer(var ABuf; ASize: UInt32); inline;
    function ReadBytes(n: Byte): DWord;
    function ReadDouble: Double;
    function ReadFloat: Single;
    function ReadFloat16: Single;
    function ReadLongString(Encoded32Method: Boolean = False): AnsiString;
    function ReadRect: TRect;
    function ReadRGB: TSwfRGB;
    function ReadRGBA: TSwfRGBA;
    function ReadSingle: Single;
    function ReadStdDouble: Double;
    function ReadString: AnsiString; overload;
    function ReadString(len: UInt32): AnsiString; overload;
    function ReadStringWithLength: AnsiString;
    function ReadSwapWord: Word;
    function ReadU16: Word;
    function ReadU30: DWord;
    function ReadU32: DWord;
    function ReadU8: Byte;
    procedure Write2Byte(w: Word);
    procedure Write4byte(dw: LongWord);
    procedure WriteBit(b: Boolean);
    procedure WriteBits(Data: Byte; Size: Byte); overload;
    procedure WriteBits(Data: LongInt; Size: Byte); overload;
    procedure WriteBits(Data: LongWord; Size: Byte); overload;
    procedure WriteBits(Data: Word; Size: Byte); overload;
    procedure WriteBuffer(const ABuf; ASize: UInt32);
    procedure WriteColor(c: TSwfRGB); overload;
    procedure WriteColor(c: TSwfRGBA); overload;
    procedure WriteEncodedString(s: AnsiString);
    procedure WriteFloat(s: Single);
    procedure WriteRect(r: TRect); overload;
    procedure WriteRect(r: TSwfRect); overload;
    procedure WriteSingle(s: Single);
    procedure WriteStdDouble(d: Double);
    procedure WriteString(s: AnsiString; NullEnd: Boolean = True; U: Boolean = False);
    procedure WriteSwapWord(w: Word);
    procedure WriteU16(w: Word);
    procedure WriteU30(Data: DWord);
    procedure WriteU32(dw: DWord);
    procedure WriteU8(b: Byte);
    procedure WriteUTF8String(s: PChar; L: Integer; NullEnd: Boolean = True);
    property BitsStream: TStream read FBitsStream;
  end;

function AddAlphaValue(c: TSwfRGBA; A: Smallint): TSwfRGBA;

function CheckBit(w: Word; n: Byte): Boolean;

function GetBitsCount(Value: LongInt; add: Byte): Byte; overload;

function GetBitsCount(Value: LongWord): Byte; overload;

function GetCubicPoint(P0, P1, P2, P3: LongInt; t: Single): Double;

function IntToSingle(w: LongInt): Single;

function MaxValue(A, b: LongInt): LongWord; overload;

function MaxValue(A, b, c, d: LongInt): LongWord; overload;

function NormalizeRect(r: TRect): TRect;

function RectToSWFRect(r: TRect; Convert: Boolean = True): TSwfRect;

function Sign(const AValue: Double): ShortInt;

function SingleToInt(s: Single): LongInt;

function SingleToWord(s: Single): Word;

function SwapColorChannels(Color: LongInt): LongInt;

function SWFRectToRect(r: TSwfRect; Convert: Boolean = True): TRect;

function SWFRGB(r, g, b: Byte): TSwfRGB; overload;

function SWFRGB(c: TColor): TSwfRGB; overload;

function SWFRGBA(r, g, b, A: Byte): TSwfRGBA; overload;

function SWFRGBA(c: TSwfRGB; A: Byte = 255): TSwfRGBA; overload;

function SWFRGBA(c: TSwfRGBA; A: Byte = 255): TSwfRGBA; overload;

function SWFRGBA(c: TColor; A: Byte = 255): TSwfRGBA; overload;

function WithoutA(c: TSwfRGBA): TSwfRGB;

function WordToSingle(w: Word): Single;

implementation

function AddAlphaValue(c: TSwfRGBA; A: Smallint): TSwfRGBA;
begin
  if (c.A + A) > 255 then
    c.A := 255
  else if (c.A + A) < 0 then
    c.A := 0
  else
    c.A := c.A + A;
end;

function CheckBit(w: Word; n: Byte): Boolean;
begin
  result := ((w shr (n - 1)) and 1) = 1;
end;

function GetBitsCount(Value: LongInt; add: Byte): Byte;
var
  n: LongWord;
begin
  if Value = 0 then
    result := 0 { + add }
  else
  begin
    n := Abs(Value);
    result := GetBitsCount(n) + add;
  end;
end;

function GetBitsCount(Value: LongWord): Byte;
var
  n: LongWord;
  il: Byte;
begin
  result := 0;
  if LongInt(Value) < 0 then
    result := 32
  else if (Value > 0) then
  begin
    n := 1;
    for il := 1 to 32 do
    begin
      n := n shl 1;
      if (n > Value) then
      begin
        result := il;
        Break;
      end;
    end;
  end;
end;

function GetCubicPoint(P0, P1, P2, P3: LongInt; t: Single): Double;
var
  t2, b, g: Double;
begin
  // P(t) = P0*(1-t)^3 + P1*3*t*(1-t)^2 + P2*3*t^2*(1-t) + P3*t^3
  t2 := t * t;
  g := 3 * (P1 - P0);
  b := 3 * (P2 - P1) - g;
  result := (P3 - P0 - b - g) * t2 * t + b * t2 + g * t + P0;
end;

// flash used 16.16 bit
function IntToSingle(w: LongInt): Single;
begin
  // Result := W shr 16 + (W and $FFFF) / ($FFFF+1);
  result := w / specFixed;
end;

function MaxValue(A, b: LongInt): LongWord;
var
  _a, _b: LongInt;
begin
  _a := Abs(A);
  _b := Abs(b);
  if (_a > _b) then
    result := _a
  else
    result := _b;
end;

function MaxValue(A, b, c, d: LongInt): LongWord;
var
  _a, _b, _c, _d: LongInt;
begin
  _a := Abs(A);
  _b := Abs(b);
  _c := Abs(c);
  _d := Abs(d);

  if (_a > _b) then
    if (_a > _c) then
      if (_a > _d) then
        result := _a
      else
        result := _d
    else if (_c > _d) then
      result := _c
    else
      result := _d
  else if (_b > _c) then
    if (_b > _d) then
      result := _b
    else
      result := _d
  else if (_c > _d) then
    result := _c
  else
    result := _d;
end;

function NormalizeRect(r: TRect): TRect;
begin
  if r.Left > r.Right then
  begin
    result.Left := r.Right;
    result.Right := r.Left;
  end
  else
  begin
    result.Left := r.Left;
    result.Right := r.Right;
  end;

  if r.Top > r.Bottom then
  begin
    result.Top := r.Bottom;
    result.Bottom := r.Top;
  end
  else
  begin
    result.Top := r.Top;
    result.Bottom := r.Bottom;
  end;
end;

function RectToSWFRect(r: TRect; Convert: Boolean = True): TSwfRect;
begin
  if r.Left < r.Right then
  begin
    result.Xmin := r.Left;
    result.Xmax := r.Right;
  end
  else
  begin
    result.Xmin := r.Right;
    result.Xmax := r.Left;
  end;

  if r.Top < r.Bottom then
  begin
    result.Ymin := r.Top;
    result.Ymax := r.Bottom;
  end
  else
  begin
    result.Ymin := r.Bottom;
    result.Ymax := r.Top;
  end;

  if Convert then
    with result do
    begin
      Xmin := Xmin * TWIPS;
      Xmax := Xmax * TWIPS;
      Ymin := Ymin * TWIPS;
      Ymax := Ymax * TWIPS;
    end;
end;

function Sign(const AValue: Double): ShortInt;
begin
  if ((PInt64(@AValue)^ and $7FFFFFFFFFFFFFFF) = $0000000000000000) then
    result := 0
  else if ((PInt64(@AValue)^ and $8000000000000000) = $8000000000000000) then
    result := -1
  else
    result := 1;
end;

function SingleToInt(s: Single): LongInt;
begin
  result := trunc(s * specFixed);
  // Result := trunc(s) shl 16 + Round(Frac(s) * ($FFFF + 1));
  // Move(s, Result, 4);
end;

function SingleToWord(s: Single): Word;
begin
  result := trunc(s) shl 8 + Round(Frac(s) * ($FF + 1));
end;

function SwapColorChannels(Color: LongInt): LongInt;
begin
  result := (Color and $FF) shl 16 + (Color and $FF00) + (Color and $FF0000) shr 16;
end;

// Convering px <=> twips ���� Convert = True
function SWFRectToRect(r: TSwfRect; Convert: Boolean = True): TRect;
begin
  with result do
  begin
    Left := r.Xmin;
    Right := r.Xmax;
    Top := r.Ymin;
    Bottom := r.Ymax;
  end;

  if Convert then
    with result do
    begin
      Left := Left div TWIPS;
      Right := Right div TWIPS;
      Top := Top div TWIPS;
      Bottom := Bottom div TWIPS;
    end;
end;

function SWFRGB(r, g, b: Byte): TSwfRGB;
begin
  result.r := r;
  result.g := g;
  result.b := b;
end;

function SWFRGB(c: TColor): TSwfRGB;
begin
  c := ColorToRGB(c);
  result := SWFRGB(GetRValue(ColorToRGB(c)), GetGValue(ColorToRGB(c)), GetBValue(ColorToRGB(c)));
end;

function SWFRGBA(r, g, b, A: Byte): TSwfRGBA;
begin
  result.r := r;
  result.g := g;
  result.b := b;
  result.A := A;
end;

function SWFRGBA(c: TSwfRGB; A: Byte): TSwfRGBA;
begin
  result.r := c.r;
  result.g := c.g;
  result.b := c.b;
  result.A := A;
end;

function SWFRGBA(c: TSwfRGBA; A: Byte = 255): TSwfRGBA;
begin
  result := c;
  result.A := A;
end;

function SWFRGBA(c: TColor; A: Byte = 255): TSwfRGBA;
begin
  result.r := GetRValue(ColorToRGB(c));
  result.g := GetGValue(ColorToRGB(c));
  result.b := GetBValue(ColorToRGB(c));
  result.A := A;
end;

function WithoutA(c: TSwfRGBA): TSwfRGB;
begin
  result := SWFRGB(c.r, c.g, c.b);
end;

// =======================================================

function WordToSingle(w: Word): Single;
begin
  result := w shr 8 + (w and $FF) / ($FF + 1);
end;

// =================== TBitsEngine ====================
constructor TBitsEngine.Create(bs: TStream);
begin
  inherited Create;
  FBitsStream := bs;
  FLastByte := 0;
  FLastBitCount := 0;
end;

// --- function for write datas for stream

procedure TBitsEngine.FlushLastByte(write: Boolean);
begin
  if FLastBitCount > 0 then
  begin
    if write then
      WriteBits(LongWord(0), 8 - FLastBitCount);
    FLastByte := 0;
    FLastBitCount := 0;
  end;
end;

function TBitsEngine.GetBits(n: Integer): DWord;
var
  s: Integer;
begin
  result := 0;
  if n > 0 then
  begin
    {
      if FLastBitCount = 0 then
      begin
      FLastByte := ReadU8; // Get the next buffer
      FLastBitCount := 8;
      end; }

    repeat
      s := n - FLastBitCount;
      if (s > 0) then
      begin // Consume the entire buffer
        result := result or (FLastByte shl s);
        n := n - FLastBitCount;
        FLastByte := ReadU8; // Get the next buffer
        FLastBitCount := 8;
      end;
    until s <= 0;

    result := result or (FLastByte shr - s);
    FLastBitCount := FLastBitCount - n;
    FLastByte := FLastByte and ($FF shr (8 - FLastBitCount)); // mask off the consumed bits
  end;
end;

function TBitsEngine.GetSBits(n: Integer): LongInt; // Get n bits from the string with sign extension
begin
  result := GetBits(n);
  // Is the number negative
  if (result and (1 shl (n - 1))) <> 0 then // Yes. Extend the sign.
    result := result or (-1 shl n);
end;

procedure TBitsEngine.ReadBuffer(var ABuf; ASize: UInt32);
begin
  FBitsStream.Read(ABuf, ASize);
end;

function TBitsEngine.ReadBytes(n: Byte): DWord;
begin
  result := 0;
  if n > 4 then
    n := 4;
  FBitsStream.Read(result, n);
end;

function TBitsEngine.ReadDouble: Double;
var
  d: Double;
  AB: array [0 .. 7] of Byte absolute d;
  il: Byte;
begin
  d := 0;
  for il := 7 downto 4 do
    AB[il] := ReadU8;
  for il := 0 to 3 do
    AB[il] := ReadU8;
  result := d;
end;

function TBitsEngine.ReadFloat: Single;
var
  LW: LongWord;
begin
  FBitsStream.Read(LW, 4);
  Move(LW, result, 4);
end;

function TBitsEngine.ReadFloat16: Single;
var
  w: Word;
  L: LongWord;
begin
  FBitsStream.Read(w, 2);
  if w = 0 then
    result := 0
  else
  begin
    L := (w and $FC00) shl 16 + (w and $3FF) shl 7;
    Move(L, result, 4);
  end;
end;

function TBitsEngine.ReadLongString(Encoded32Method: Boolean = False): AnsiString;
var
  c, il: Word;
begin
  if Encoded32Method then
    c := ReadU30
  else
    c := ReadSwapWord;
  result := '';
  if c > 0 then
  begin
    if c > 255 then
      for il := 1 to c do
        result := result + AnsiChar(ReadU8)
    else
      result := ReadString(c);
  end;
end;

function TBitsEngine.ReadRect: TRect;
var
  nBits: Byte;
begin
  nBits := GetBits(5);
  result.Left := GetSBits(nBits);
  result.Right := GetSBits(nBits);
  result.Top := GetSBits(nBits);
  result.Bottom := GetSBits(nBits);
  FlushLastByte(False);
end;

function TBitsEngine.ReadRGB: TSwfRGB;
begin
  with FBitsStream do
  begin
    read(result.r, 1);
    read(result.g, 1);
    read(result.b, 1);
  end;
end;

function TBitsEngine.ReadRGBA: TSwfRGBA;
begin
  with FBitsStream do
  begin
    read(result.r, 1);
    read(result.g, 1);
    read(result.b, 1);
    read(result.A, 1);
  end;
end;

function TBitsEngine.ReadSingle: Single;
var
  LW: LongInt;
begin
  FBitsStream.Read(LW, 4);
  result := IntToSingle(LW);
end;

function TBitsEngine.ReadStdDouble: Double;
begin
  FBitsStream.Read(result, 8);
end;

function TBitsEngine.ReadString: AnsiString;
var
  b: Byte;
begin
  result := '';
  repeat
    b := ReadU8;
    if b <> 0 then
      result := result + AnsiChar(b);
  until b = 0;
end;

function TBitsEngine.ReadString(len: UInt32): AnsiString;
begin
  if len = 0 then
    Exit('');

  SetLength(Result, len);
  FBitsStream.Read(Result[1], len);
end;

function TBitsEngine.ReadStringWithLength: AnsiString;
var
  len: UInt32;
begin
  len := ReadU30();
  if len = 0 then
    Exit('');

  Result := ReadString(len);
end;

function TBitsEngine.ReadSwapWord: Word;
begin
  FBitsStream.Read(result, 2);
  result := (result and $FF) shl 8 + result shr 8;
end;

function TBitsEngine.ReadU16: Word;
begin
  FBitsStream.Read(result, 2);
end;

function TBitsEngine.ReadU30: DWord;
var
  b, il: Byte;
begin
  il := 0;
  result := 0;
  while il < 5 do
  begin
    b := ReadU8;
    result := result + (b and $7F) shl (il * 7);
    if CheckBit(b, 8) then
      inc(il)
    else
      il := 5;
  end;
end;

function TBitsEngine.ReadU32: DWord;
begin
  FBitsStream.Read(result, 4);
end;

function TBitsEngine.ReadU8: Byte;
begin
  FBitsStream.Read(result, 1);
end;

procedure TBitsEngine.Write2Byte(w: Word);
begin
  WriteBits(w, 16);
end;

procedure TBitsEngine.Write4byte(dw: LongWord);
begin
  FlushLastByte;
  FBitsStream.Write(dw, 4);
end;

procedure TBitsEngine.WriteBit(b: Boolean);
begin
  WriteBits(LongWord(b), 1);
end;

procedure TBitsEngine.WriteBits(Data: Byte; Size: Byte);
begin
  WriteBits(LongWord(Data), Size);
end;

procedure TBitsEngine.WriteBits(Data: LongInt; Size: Byte);
begin
  WriteBits(LongWord(Data), Size);
end;

procedure TBitsEngine.WriteBits(Data: LongWord; Size: Byte);
type
  T4B = array [0 .. 3] of Byte;

var
  tmpDW: LongWord;
  tmp4b: T4B absolute tmpDW;

  cwbyte, totalBits, il: Byte;
  endBits: Byte;
begin
  if Size = 0 then
    Exit;

  // clear bits at the left if negative
  tmpDW := Data shl (32 - Size) shr (32 - Size);

  totalBits := FLastBitCount + Size;
  cwbyte := (totalBits) div 8;
  if cwbyte = 0 then // if empty byte
  begin
    FLastByte := (FLastByte shl Size) or tmpDW;
    FLastBitCount := totalBits;
  end
  else
  begin
    endBits := totalBits mod 8;
    if endBits = 0 then // is full
    begin
      tmpDW := tmpDW + (FLastByte shl Size);
      FLastByte := 0;
      FLastBitCount := 0;
    end
    else // rest
    begin
      tmpDW := FLastByte shl (Size - endBits) + (tmpDW shr endBits);
      FLastBitCount := endBits;
      FLastByte := Byte(Data shl (8 - endBits)) shr (8 - endBits);
    end;

    for il := cwbyte downto 1 do
      FBitsStream.Write(tmp4b[il - 1], 1);
  end;
end;

procedure TBitsEngine.WriteBits(Data: Word; Size: Byte);
begin
  WriteBits(LongWord(Data), Size);
end;

procedure TBitsEngine.WriteBuffer(const ABuf; ASize: UInt32);
begin
  FBitsStream.Write(ABuf, ASize);
end;

procedure TBitsEngine.WriteColor(c: TSwfRGB);
begin
  with FBitsStream do
  begin
    write(c.r, 1);
    write(c.g, 1);
    write(c.b, 1);
  end;
end;

procedure TBitsEngine.WriteColor(c: TSwfRGBA);
begin
  with FBitsStream do
  begin
    write(c.r, 1);
    write(c.g, 1);
    write(c.b, 1);
    write(c.A, 1);
  end;
end;

procedure TBitsEngine.WriteEncodedString(s: AnsiString);
var
  SLen, il: Word;
begin
  SLen := length(s);
  WriteU30(SLen);
  if SLen > 0 then
    for il := 1 to SLen do
      WriteU8(Byte(s[il]));
end;

procedure TBitsEngine.WriteFloat(s: Single);
var
  LW: LongWord;
begin
  Move(s, LW, 4);
  WriteU32(LW);
end;

procedure TBitsEngine.WriteRect(r: TRect);
begin
  WriteRect(RectToSWFRect(r, False));
end;

procedure TBitsEngine.WriteRect(r: TSwfRect);
var
  nBits: Byte;
begin
  nBits := GetBitsCount(MaxValue(r.Xmin, r.Xmax, r.Ymin, r.Ymax), 1);
  WriteBits(nBits, 5);
  WriteBits(r.Xmin, nBits);
  WriteBits(r.Xmax, nBits);
  WriteBits(r.Ymin, nBits);
  WriteBits(r.Ymax, nBits);
  FlushLastByte;
end;

procedure TBitsEngine.WriteSingle(s: Single);
var
  LW: LongInt;
begin
  LW := SingleToInt(s);
  WriteU32(LW);
end;

procedure TBitsEngine.WriteStdDouble(d: Double);
begin
  FBitsStream.Write(d, 8);
end;

procedure TBitsEngine.WriteString(s: AnsiString; NullEnd: Boolean = True; U: Boolean = False);
var
  il, len: Integer;
  tmpS: RawByteString;
begin
  if U then
  begin
    tmpS := AnsiToUTF8(string(s));
    len := length(tmpS);
    if len > 0 then
      for il := 1 to len do
        WriteU8(Ord(tmpS[il]));
  end
  else
  begin
    len := length(s);
    if len > 0 then
      for il := 1 to len do
        WriteU8(Byte(s[il]));
  end;
  if NullEnd then
    WriteU8(0);
end;

procedure TBitsEngine.WriteSwapWord(w: Word);
begin
  FlushLastByte;
  WriteU8(HiByte(w));
  WriteU8(LoByte(w));
end;

procedure TBitsEngine.WriteU16(w: Word);
begin
  FlushLastByte;
  FBitsStream.Write(w, 2);
end;

procedure TBitsEngine.WriteU30(Data: DWord);
var
  AByte: array [0 .. 3] of Byte absolute Data;
  il, Max: Byte;
begin
  if Data = 0 then
    WriteU8(0)
  else
  begin
    Max := 3;
    while (Max > 0) and (AByte[Max] = 0) do
      dec(Max);
    for il := 0 to Max do
    begin
      WriteBit((il < Max) or ((GetBitsCount(AByte[il]) + il) > 7));
      WriteBits(AByte[il], 8 - il - 1);
      if il > 0 then
        WriteBits(Byte(AByte[il - 1] shr (8 - il)), il);
    end;
    if ((GetBitsCount(Data) + Max) > ((Max + 1) * 7)) and ((AByte[Max] shr (8 - Max - 1)) > 0) then
      WriteU8(Byte(AByte[Max] shr (8 - Max - 1)));
  end;
end;

procedure TBitsEngine.WriteU32(dw: DWord);
begin
  FlushLastByte;
  FBitsStream.Write(dw, 4);
end;

procedure TBitsEngine.WriteU8(b: Byte);
begin
  if FLastBitCount = 0 then
    FBitsStream.Write(b, 1)
  else
    WriteBits(b, 8);
end;

procedure TBitsEngine.WriteUTF8String(s: PChar; L: Integer; NullEnd: Boolean = True);
var
  il: Integer;
begin
  if L > 0 then
    for il := 0 to L - 1 do
      WriteU8(Byte(s[il]));
  if NullEnd then
    WriteU8(0);
end;

end.

unit Vin.Swf.Types;

interface

type
  { Integer Types }

  TBitWidth = Integer;

  SI8 = Int8;
  SI16 = Int16;
  SI32 = Int32;

  UI8 = UInt8;
  UI16 = UInt16;
  UI32 = UInt32;
  UI64 = UInt64;

  { Float Types }

  { SWF Header }

  TSwfRect = packed record
    Xmin: Integer; // in twips
    Xmax: Integer; // in twips
    Ymin: Integer; // in twips
    Ymax: Integer; // in twips
  end;

  TSwfRGB = packed record
    R: UI8;
    G: UI8;
    B: UI8;
  end;

  TSwfRGBA = packed record
    R: UI8;
    G: UI8;
    B: UI8;
    A: UI8;
  end;

  TSwfARGB = packed record
    Alpha: UI8;
    Red: UI8;
    Green: UI8;
    Blue: UI8;
  end;

implementation

end.

object MainForm: TMainForm
  Left = 0
  Top = 0
  ActiveControl = wb
  BorderWidth = 5
  Caption = 'Swf Viewer'
  ClientHeight = 502
  ClientWidth = 918
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object mmLog: TMemo
    Left = 0
    Top = 384
    Width = 918
    Height = 118
    Align = alBottom
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 918
    Height = 376
    ActivePage = TabSheet5
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Swf Viewer'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter1: TSplitter
        Left = 161
        Top = 41
        Height = 307
        ExplicitLeft = 216
        ExplicitTop = 104
        ExplicitHeight = 100
      end
      object lvTags: TListView
        Left = 0
        Top = 41
        Width = 161
        Height = 307
        Align = alLeft
        Columns = <
          item
            AutoSize = True
            Caption = 'Tag Name'
          end>
        ReadOnly = True
        RowSelect = True
        TabOrder = 0
        ViewStyle = vsReport
        OnClick = lvTagsClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 910
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object btnLoadSWF: TButton
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Load SWF'
          TabOrder = 0
          OnClick = btnLoadSWFClick
        end
        object btnSaveSWF: TButton
          Left = 89
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Save SWF'
          TabOrder = 1
          OnClick = btnSaveSWFClick
        end
        object btnDumpDoABCTag: TButton
          Left = 170
          Top = 8
          Width = 112
          Height = 25
          Caption = 'Dump DoABC Tag'
          TabOrder = 2
          OnClick = btnDumpDoABCTagClick
        end
        object btnHack: TButton
          Left = 549
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Hack'
          TabOrder = 3
          OnClick = btnHackClick
        end
        object btnLoadDumpedSWF: TButton
          Left = 345
          Top = 8
          Width = 112
          Height = 25
          Caption = 'Load Dumped Swf'
          TabOrder = 4
          OnClick = btnLoadDumpedSWFClick
        end
      end
      object Panel1: TPanel
        Left = 164
        Top = 41
        Width = 746
        Height = 307
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 2
        object pTagInfo: TPanel
          Left = 1
          Top = 1
          Width = 744
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object lbVer: TLabel
            Left = 0
            Top = 0
            Width = 30
            Height = 12
            Align = alLeft
            Caption = 'lbVer'
            Layout = tlCenter
          end
        end
        object HexEdit: TCnHexEditor
          Left = 1
          Top = 26
          Width = 744
          Height = 280
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Courier New'
          Font.Style = []
          ReadOnly = True
          ChangeDataSize = False
          ParentFont = False
          ParentColor = False
          TabOrder = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Abc Viewer'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 910
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnLoadAbcFile: TButton
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Load ABC'
          TabOrder = 0
          OnClick = btnLoadAbcFileClick
        end
        object btnSaveABC: TButton
          Left = 89
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Save ABC'
          TabOrder = 1
          OnClick = btnSaveABCClick
        end
      end
      object tlABC: TcxTreeList
        Left = 0
        Top = 41
        Width = 910
        Height = 307
        Align = alClient
        Bands = <
          item
          end>
        OptionsBehavior.Sorting = False
        OptionsSelection.CellSelect = False
        OptionsView.ColumnAutoWidth = True
        TabOrder = 1
        object tlABCColumn1: TcxTreeListColumn
          Caption.Text = 'Index'
          DataBinding.ValueType = 'String'
          Width = 300
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn2: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Width = 300
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn3: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Width = 300
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn4: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Width = 300
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn5: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 4
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn6: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 5
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn7: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 6
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn8: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 7
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn9: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 8
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object tlABCColumn10: TcxTreeListColumn
          DataBinding.ValueType = 'String'
          Position.ColIndex = 9
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'RF Viewer'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 910
        Height = 65
        Align = alTop
        TabOrder = 0
        object edRFVersion: TLabeledEdit
          Left = 88
          Top = 8
          Width = 145
          Height = 20
          EditLabel.Width = 42
          EditLabel.Height = 12
          EditLabel.Caption = 'version'
          LabelPosition = lpLeft
          TabOrder = 0
        end
        object edRFPvtKey: TLabeledEdit
          Left = 88
          Top = 35
          Width = 145
          Height = 20
          EditLabel.Width = 66
          EditLabel.Height = 12
          EditLabel.Caption = 'private key'
          LabelPosition = lpLeft
          TabOrder = 1
          Text = 'opwilj527yomna9812unx'
        end
        object edRFPubKey: TLabeledEdit
          Left = 328
          Top = 35
          Width = 337
          Height = 20
          EditLabel.Width = 60
          EditLabel.Height = 12
          EditLabel.Caption = 'public key'
          LabelPosition = lpLeft
          TabOrder = 2
        end
        object edRFFile: TLabeledEdit
          Left = 328
          Top = 8
          Width = 337
          Height = 20
          EditLabel.Width = 42
          EditLabel.Height = 12
          EditLabel.Caption = 'rf file'
          LabelPosition = lpLeft
          TabOrder = 3
          Text = 'D:\Develop\My Apps\SwfViewer\bin\'#25705#30331#22478#24066'\Data.RF'
        end
        object btnLoadRF: TButton
          Left = 679
          Top = 8
          Width = 75
          Height = 51
          Caption = 'Load'
          TabOrder = 4
          OnClick = btnLoadRFClick
        end
        object btnSaveSelected: TButton
          Left = 768
          Top = 6
          Width = 105
          Height = 25
          Caption = 'btnSaveSelected'
          TabOrder = 5
          OnClick = btnSaveSelectedClick
        end
      end
      object tvRF: TTreeView
        Left = 0
        Top = 65
        Width = 910
        Height = 283
        Align = alClient
        Indent = 19
        TabOrder = 1
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Hack Test'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 138
        Height = 12
        Caption = #35268#21017#65288#20256#32473'SetConfig'#30340#65289
      end
      object Label2: TLabel
        Left = 160
        Top = 325
        Width = 228
        Height = 12
        Caption = #36755#20986#25991#20214#26159' SaveSwf '#30446#24405#19979#30340' Hacked.swf'
      end
      object mmRules: TMemo
        Left = 16
        Top = 35
        Width = 881
        Height = 158
        Lines.Strings = (
          '<maps>'
          #9'<map url="/Loading.swf" local="test1.swf" match="middle" />'
          #9'<map url="/film.swf" local="into.swf" match="middle" />'
          #9'<map url="/SWCLib.swf" local="into.swf" match="middle" />'
          #9'<map url="/index.swf" local="test1.swf" match="middle" />'
          #9'<map url="/mimes.txt" local="'#39764#25106#19977#37096#26354'.txt" match="middle" />'
          '</maps>')
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object btnLoadAndHack: TButton
        Left = 16
        Top = 312
        Width = 121
        Height = 33
        Caption = #25171#24320'SWF'#24182#22788#29702
        TabOrder = 1
        OnClick = btnLoadAndHackClick
      end
      object edRFFile2: TLabeledEdit
        Left = 16
        Top = 224
        Width = 400
        Height = 20
        EditLabel.Width = 174
        EditLabel.Height = 12
        EditLabel.Caption = #36164#28304#25991#20214#21253#65288#20256#32473'SetConfig'#30340#65289
        TabOrder = 2
      end
      object edRFPass2: TLabeledEdit
        Left = 440
        Top = 224
        Width = 400
        Height = 20
        EditLabel.Width = 186
        EditLabel.Height = 12
        EditLabel.Caption = #36164#28304#25991#20214#23494#30721#65288#20256#32473'SetConfig'#30340#65289
        TabOrder = 3
      end
      object edSrcUrl: TLabeledEdit
        Left = 16
        Top = 269
        Width = 400
        Height = 20
        EditLabel.Width = 216
        EditLabel.Height = 12
        EditLabel.Caption = 'Flash'#30340#26469#28304'URL'#65288#21028#26029#26159#21542#38656#35201#22788#29702#30340#65289
        TabOrder = 4
        Text = 'http://xxx/Loading.swf?yyy'
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'TabSheet5'
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object wb: TWebBrowser
        Left = 0
        Top = 41
        Width = 910
        Height = 307
        Align = alClient
        TabOrder = 0
        ExplicitLeft = -264
        ExplicitTop = 153
        ControlData = {
          4C0000000D5E0000BB1F00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 910
        Height = 41
        Align = alTop
        TabOrder = 1
        object Button1: TButton
          Left = 255
          Top = 10
          Width = 75
          Height = 25
          Caption = 'Test'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 71
          Top = 10
          Width = 75
          Height = 25
          Caption = 'Hook'
          TabOrder = 1
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 152
          Top = 10
          Width = 75
          Height = 25
          Caption = 'UnHook'
          TabOrder = 2
          OnClick = Button3Click
        end
        object edTestUrl: TEdit
          Left = 336
          Top = 14
          Width = 417
          Height = 20
          Hint = 
            'http://cdn.dl.7.qq.com/dynasty/swf/Loading.swf?a058aaa774e25a8bf' +
            '41b35eda77fffd9'
          TabOrder = 3
          Text = 'http://www.lyyfk.com/index.swf'
        end
        object Button4: TButton
          Left = 8
          Top = 10
          Width = 57
          Height = 25
          Caption = 'Set'
          TabOrder = 4
          OnClick = Button4Click
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 376
    Width = 918
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    HotZone.SizePercent = 40
    AlignSplitter = salBottom
    Control = mmLog
    ExplicitWidth = 8
  end
  object dlgOpen: TOpenDialog
    Filter = 'Flash|*.swf|Action Script Byte Code|*.abc'
    Left = 336
    Top = 376
  end
  object dlgSave: TSaveDialog
    Filter = 'Flash|*.swf|Action Script Byte Code|*.abc'
    Left = 392
    Top = 376
  end
end

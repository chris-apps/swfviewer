unit Vin.Swf.AbcFile.Consts;

interface

const
  // Multiname Kind
  CONSTANT_QName = $07;
  CONSTANT_QNameA = $0D;
  CONSTANT_RTQName = $0F;
  CONSTANT_RTQNameA = $10;
  CONSTANT_RTQNameL = $11;
  CONSTANT_RTQNameLA = $12;
  CONSTANT_Multiname = $09;
  CONSTANT_MultinameA = $0E;
  CONSTANT_MultinameL = $1B;
  CONSTANT_MultinameLA = $1C;
  CONSTANT_GenericName = $1D;

  // Constant kind
  CONSTANT_Int = $03;     // integer
  CONSTANT_UInt = $04;    // uinteger
  CONSTANT_Double = $06;  // double
  CONSTANT_Utf8 = $01;    // string
  CONSTANT_True = $0B;    // -
  CONSTANT_False = $0A;   // -
  CONSTANT_Null = $0C;    // -
  CONSTANT_Undefined = $00;  // -
  CONSTANT_Namespace = $08;  // namespace
  CONSTANT_PackageNamespace = $16;    // namespace
  CONSTANT_PackageInternalNs = $17;   // Namespace
  CONSTANT_ProtectedNamespace = $18;  // Namespace
  CONSTANT_ExplicitNamespace = $19;   // Namespace
  CONSTANT_StaticProtectedNs = $1A;   // Namespace
  CONSTANT_PrivateNs = $05;  // namespace

  // Method signature flags
  METHOD_NEED_ARGUMENTS = $01;
  METHOD_NEED_ACTIVATION = $02;
  METHOD_NEED_REST = $04;
  METHOD_HAS_OPTIONAL = $08;
  METHOD_SET_DXNS = $40;
  METHOD_HAS_PARAM_NAMES = $80;

  // Class flags
  CONSTANT_ClassSealed = $01;
  CONSTANT_ClassFinal = $02;
  CONSTANT_ClassInterface = $04;
  CONSTANT_ClassProtectedNs = $08;

  // Trait types
  CONSTANT_Trait_Slot = 0;
  CONSTANT_Trait_Method = 1;
  CONSTANT_Trait_Getter = 2;
  CONSTANT_Trait_Setter = 3;
  CONSTANT_Trait_Class = 4;
  CONSTANT_Trait_Function = 5;
  CONSTANT_Trait_Const = 6;

  // Trait attributes
  CONSTANT_Trait_ATTR_Final = $1;
  CONSTANT_Trait_ATTR_Override = $2;
  CONSTANT_Trait_ATTR_Metadata = $4;

implementation

end.

unit uSwfHacker;

interface

uses
  Windows, SysUtils, Classes, msxml, StrUtils, Generics.Collections, RegularExpressions,
  vxUtils, vxXmlDom, Vin.Swf, Vin.Swf.Consts, Vin.Swf.AbcFile, Vin.Swf.AbcFile.Consts, uVBRes;

const
  RES_ONLY = TRUE;

type
  TSwfHackRule = record
    Package: string;
    Class_: string;
    Func: string;
  end;

  TSwfHackRules = TList<TSwfHackRule>;

  TMapItem = class
    url: string;
    local: string;
    match: string;
    toPub: Boolean;
    toSave: Boolean;
    hackRules: TSwfHackRules;
  public
    constructor Create(ANode: IXMLDOMNode);
  end;

  TUrlMap = class
  private
    FList: TList<TMapItem>;
  public
    constructor Create;
    function IsMatch(ASourceUrl: string; var AMapItem: TMapItem): Boolean;
    procedure LoadFromXml(const AXmlText: string);
  end;

  TAbcFileHelper = class helper for TAbcFile
  public
    function MultinameQNSToString(id: Int32): string;
    function MultinameQToString(id: Int32): string;
  end;

type
  TByteDynArray = array of byte;

procedure SetConfig(const AXmlText, APackFile, APackPass: PAnsiChar); stdcall;
function NeedConvert(ASourceUrl: string): Boolean;
function ConvertSwf(var ABuffer: TByteDynArray): BOOL; overload;
function ConvertSwf(var ms: TMemoryStream): BOOL; overload;

procedure HackSwf(AStream: TStream; ARules: TSwfHackRules);

procedure TryGetLocalFile(var ms: TMemoryStream);

var
  FileCounter: Int32 = 0;
  CurMapItem: TMapItem;
  UseResFile: Boolean;
  Res: TResFiles = nil;

implementation

var
  LUrlMap: TUrlMap;
  LPackFile: string;
  LPackPass: AnsiString;
  LLastUrl: string;
  LConfigLoaded: Boolean = false;

procedure SetConfig(const AXmlText, APackFile, APackPass: PAnsiChar); stdcall;
var
  LXmlText: string;
begin
  if LConfigLoaded then Exit;

  LXmlText := string(AXmlText);
  LUrlMap.LoadFromXml(LXmlText);
  LPackFile := string(AnsiString(APackFile));
  LPackPass := AnsiString(APackPass);

  UseResFile := (Length(LPackFile) > 0) and (Length(LPackPass) > 0);
  if UseResFile then
  begin
    Res := GetResFilesByFile(LPackFile, LPackPass);
{$IFDEF DEBUG}
    if Res = nil then
      OutputDebugString('加密包启动失败！');
{$ENDIF}
  end;
  LConfigLoaded := True;
end;

function NeedConvert(ASourceUrl: string): Boolean;
begin
  LLastUrl := ASourceUrl;
  Result := LUrlMap.IsMatch(ASourceUrl, CurMapItem);
{$IFDEF DEBUG}
  OutputDebugString(PChar('请求检测是否要处理：' + ASourceUrl + '   检测结果:' + BoolToStr(Result, true)));
{$ENDIF}
end;

function ConvertSwf(var ABuffer: TByteDynArray): BOOL; overload;
var
  ms: TMemoryStream;
begin
  if Length(ABuffer) = 0 then Exit(False);
  ms := TMemoryStream.Create;
  try
    ms.Write(ABuffer[0], Length(ABuffer));
    Result := ConvertSwf(ms);
    ms.Position := 0;
    SetLength(ABuffer, ms.Size);
    ms.Read(ABuffer[0], ms.Size);
  finally
    ms.Free;
  end;
end;

procedure TryGetLocalFile(var ms: TMemoryStream);
var
  rf: TResFile;
begin
  ms.Clear;
  {$IFDEF DEBUG}
  OutputDebugString(PChar('规则定义的本地文件： '+CurMapItem.local));
  {$ENDIF}
  if not UseResFile then   // 没有定义加密包
  begin
    if FileExists(CurMapItem.local) then
    begin
      ms.LoadFromFile(CurMapItem.local);
      {$IFDEF DEBUG}
      OutputDebugString(PChar('成功加载本地文件 '+CurMapItem.local));
      {$ENDIF}
    end
    else
    begin
      {$IFDEF DEBUG}
      OutputDebugString(PChar('加载本地文件 '+CurMapItem.local+' 失败，文件不存在。'));
      {$ENDIF}
    end;
  end
  else if Res <> nil then         // 加密包启动成功
  begin
    rf := Res.FindFile(AnsiString(CurMapItem.local));
    if rf <> nil then
    begin
      {$IFDEF DEBUG}
      OutputDebugStringA(PAnsiChar('加载加密包内的文件 '+ rf.Name));
      {$ENDIF}
      ms.Write(rf.Data[0], rf.Length);
    end;
    {$IFDEF DEBUG}
    if rf = nil then
      OutputDebugStringA(PAnsiChar('加载加密包内的文件 '+ rf.Name + ' 失败，没有找到。'))
    else
      OutputDebugString(PChar('加密包内的文件已加载，长度 ' + IntToStr(ms.Size)));
    {$ENDIF}
  end;
end;

function ConvertSwf(var ms: TMemoryStream): BOOL;
//var
//  s, fn: string;
//  i, j: Int32;
//  rf: TResFile;
begin
  if ms.Size = 0 then Exit(False);
  ms.Position := 0;
  {$IFDEF DEBUG}
  OutputDebugString(PChar(Format('请求处理SWF，URL=%s, 长度=%d', [LLastURL, ms.Size])));
(*
  if CurMapItem.toSave then
  begin
    s := GetCurrentDir + '\SaveSwf\'+ FormatDateTime('yyyymmdd', Now) + '\';
    ForceDirectories(s);
    j := Length(LLastUrl);
    for i := j downto 1 do
    begin
      if LLastUrl[i] = '?' then
        j := i-1;
      if LLastUrl[i] = '/' then
      begin
        fn := Copy(LLastUrl, i+1, j-i);
        Break;
      end;
    end;
    s := s + Format('%.2d', [FileCounter]) + '.' + fn;
    ms.SaveToFile(s);
    Inc(FileCounter);
  end;
*)
  {$ENDIF}
{
  if Length(CurMapItem.local) > 0 then
  begin
    TryGetLocalFile(ms);
  end;
}
  if CurMapItem.toPub then
  begin
    {$IFDEF DEBUG}
    OutputDebugString('请求黑掉SWF');
    {$ENDIF}
    HackSwf(ms, CurMapItem.hackRules);
    {$IFDEF DEBUG}
    ms.SaveToFile('SaveSwf\Hacked.swf');
    OutputDebugString('结束黑掉SWF');
    {$ENDIF}
  end;

  Result := True;
  {$IFDEF DEBUG}
  //ms.SaveToFile('SaveSwf\DebugSwf2.swf');
  OutputDebugString('结束处理SWF');
  {$ENDIF}
end;

constructor TUrlMap.Create;
begin
  FList := TList<TMapItem>.Create();
end;

function TUrlMap.IsMatch(ASourceUrl: string; var AMapItem: TMapItem): Boolean;
var
  i: Int32;
  mi: TMapItem;
  LSourceUrl: string;
begin
  Result := False;
  AMapItem := nil;
  LSourceUrl := LowerCase(ASourceUrl);
  for i := 0 to FList.Count-1 do
  begin
    mi := FList[i];
    if (AnsiSameText(mi.match, 'left') and (Pos(mi.url, LSourceUrl) = 1)) or
       (AnsiSameText(mi.match, 'right') and AnsiSameText(RightStr(LSourceUrl, Length(mi.url)), mi.url)) or
       (AnsiSameText(mi.match, 'regex') and TRegEx.IsMatch(LSourceUrl, mi.url)) or
       (Pos(mi.url, LSourceUrl) > 1) then
    begin
        AMapItem := mi;
        Exit(True);
    end
  end;
end;

procedure TUrlMap.LoadFromXml(const AXmlText: string);
var
  xml: IXMLDOMDocument;
  i: Int32;
  nodes: IXMLDOMNodeList;
  mi: TMapItem;
begin
  FList.Clear;
  xml := CoDOMDocument.Create;
  xml.loadXML('<?xml version="1.0" encoding="gb2312"?>'#13#10+AXmlText);
  nodes := xml.documentElement.childNodes;
  if nodes.length > 0 then
  begin
    for i := 0 to nodes.length-1 do
    begin
      mi := TMapItem.Create(nodes.item[i]);
      FList.Add(mi);
    end;
  end;
  nodes := nil;
  xml := nil;
end;

constructor TMapItem.Create(ANode: IXMLDOMNode);
var
  i: Int32;
  ruleNodes: IXMLDOMNodeList;
  rule: TSwfHackRule;
begin
  url := LowerCase(GetAttrString(ANode, 'url'));
  local := GetAttrString(ANode, 'local');
  match := GetAttrString(ANode, 'match');
  toPub := GetAttrBoolean(ANode, 'topub', False);
  toSave := GetAttrBoolean(ANode, 'tosave', False);
  hackRules := TList<TSwfHackRule>.Create();

  ruleNodes := ANode.selectNodes('hack');
  for i := 0 to RuleNodes.length-1 do
  begin
    rule.Package := GetAttrString(RuleNodes.item[i], 'package');
    rule.Class_ := GetAttrString(RuleNodes.item[i], 'class');
    rule.Func := GetAttrString(RuleNodes.item[i], 'func');

    hackRules.Add(rule);
  end;
end;

procedure HackSwf(AStream: TStream; ARules: TSwfHackRules);
var
  abc: TAbcFile;
  rule: TSwfHackRule;
  AllPackages, AllClasses, AllFuncs: Boolean;

  procedure CheckTraits(Traits: TList<TAbcTrait>);
  var
    k, kind: Int32;
    tr: TAbcTrait;
  begin
    for k := 0 to Traits.Count-1 do
    begin
      tr := Traits[k];
      if (tr.Kind in [CONSTANT_Trait_Method, CONSTANT_Trait_Function, CONSTANT_Trait_Slot]) then
      begin
        if AllFuncs and not HasValue(tr.Attr, CONSTANT_Trait_ATTR_Override) or
           (abc.MultinameQToString(tr.Name) = rule.Func) then
        begin
          kind := abc.ConstPool.Namespaces[TAbcMN_QName(abc.ConstPool.Multinames[tr.Name]).NS].Kind;
          if (kind = CONSTANT_PrivateNs) or (kind = CONSTANT_ProtectedNamespace) then
            TAbcMN_QName(abc.ConstPool.Multinames[tr.Name]).NS := 1;
        end;
      end;
    end;
  end;
var
  swf: TSwf;
  doAbc: TSwfDoABCTag;
  i, j: Int32;
  inst: TAbcInstance;
  cls: TAbcClassInfo;
begin
  swf := TSwf.Create;
  swf.LoadFromStream(AStream);
  doAbc := nil;
  for i := 0 to swf.TagCount-1 do
  begin
    if swf.Tags[i].Header.TypeID = tagDoABC then
    begin
      doAbc := TSwfDoABCTag(swf.Tags[i]);
      Break;
    end;
  end;
  if doAbc = nil then
  begin
    swf.Free;
    Exit;
  end;

  abc := doAbc.AbcFile;

  for i := 0 to ARules.Count-1 do
  begin
    rule := ARules[i];
    AllPackages := Length(rule.Package) = 0;
    AllClasses := Length(rule.Class_) = 0;
    AllFuncs := Length(rule.Func) = 0;

    for j := 0 to abc.Instances.Count-1 do
    begin
      inst := abc.Instances[j];
      if AllPackages or (abc.MultinameQNSToString(inst.Name) = rule.Package) then  // 规则的包名为空或者匹配
      begin
        if AllClasses or (abc.MultinameQToString(inst.Name) = rule.Class_) then    // 规则的类名为空或者匹配
        begin
          CheckTraits(inst.Traits);
          cls := abc.Classes[j];
          CheckTraits(cls.Traits);
        end;
      end;

    end;
  end;

  swf.SaveToStream(AStream);
end;

function TAbcFileHelper.MultinameQNSToString(id: Int32): string;
begin
  Result := ConstPool.Strings[
              ConstPool.Namespaces[
                TAbcMN_QName(ConstPool.Multinames[id]).NS].Name];
end;

function TAbcFileHelper.MultinameQToString(id: Int32): string;
begin
  Result := ConstPool.Strings[TAbcMN_QName(ConstPool.Multinames[id]).Name];
end;

initialization
  LUrlMap := TUrlMap.Create;

end.


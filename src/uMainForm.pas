unit uMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, CnHexEditor, vxUtils,
  Vin.Swf, Vin.Swf.AbcFile, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL,
  cxTextEdit, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxSplitter, OleCtrls, SHDocVw;

type
  TMainForm = class(TForm)
    btnDumpDoABCTag: TButton;
    btnHack: TButton;
    btnLoadAbcFile: TButton;
    btnLoadDumpedSWF: TButton;
    btnLoadSWF: TButton;
    btnSaveSWF: TButton;
    dlgOpen: TOpenDialog;
    dlgSave: TSaveDialog;
    lvTags: TListView;
    mmLog: TMemo;
    PageControl1: TPageControl;
    Panel2: TPanel;
    Panel3: TPanel;
    Splitter1: TSplitter;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    tlABC: TcxTreeList;
    tlABCColumn1: TcxTreeListColumn;
    tlABCColumn2: TcxTreeListColumn;
    tlABCColumn3: TcxTreeListColumn;
    tlABCColumn4: TcxTreeListColumn;
    tlABCColumn5: TcxTreeListColumn;
    tlABCColumn6: TcxTreeListColumn;
    tlABCColumn7: TcxTreeListColumn;
    tlABCColumn8: TcxTreeListColumn;
    tlABCColumn9: TcxTreeListColumn;
    tlABCColumn10: TcxTreeListColumn;
    Panel1: TPanel;
    pTagInfo: TPanel;
    btnSaveABC: TButton;
    lbVer: TLabel;
    HexEdit: TCnHexEditor;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    edRFVersion: TLabeledEdit;
    edRFPvtKey: TLabeledEdit;
    edRFPubKey: TLabeledEdit;
    edRFFile: TLabeledEdit;
    btnLoadRF: TButton;
    tvRF: TTreeView;
    btnSaveSelected: TButton;
    TabSheet4: TTabSheet;
    mmRules: TMemo;
    Label1: TLabel;
    btnLoadAndHack: TButton;
    edRFFile2: TLabeledEdit;
    edRFPass2: TLabeledEdit;
    cxSplitter1: TcxSplitter;
    edSrcUrl: TLabeledEdit;
    Label2: TLabel;
    TabSheet5: TTabSheet;
    wb: TWebBrowser;
    Panel5: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    edTestUrl: TEdit;
    Button4: TButton;
    procedure btnDumpDoABCTagClick(Sender: TObject);
    procedure btnHackClick(Sender: TObject);
    procedure btnLoadAbcFileClick(Sender: TObject);
    procedure btnLoadDumpedSWFClick(Sender: TObject);
    procedure btnLoadRFClick(Sender: TObject);
    procedure btnLoadSWFClick(Sender: TObject);
    procedure btnSaveABCClick(Sender: TObject);
    procedure btnSaveSelectedClick(Sender: TObject);
    procedure btnSaveSWFClick(Sender: TObject);
    procedure btnLoadAndHackClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lvTagsClick(Sender: TObject);
  private
    //FabcFile: TAbcFile;
    FCurTag: TSwfTag;
    FSwf: TSwf;
    FTagNames: TStringList;
    FTagStream: TMemoryStream;
    procedure LoadSWF(const AFileName: string);
  end;

var
  MainForm: TMainForm;

implementation

uses
  Vin.Swf.AbcFile.Consts;
  //uSwfHacker, uVBRes;

const
  libswfhack_dll = 'LHH.dll';
procedure HookApi; stdcall; external libswfhack_dll name 'HookApi';
procedure UnHookApi; stdcall; external libswfhack_dll name 'UnHookApi';
procedure SetConfig(const AXmlText, APackFile, APackPass: PAnsiChar); stdcall; external libswfhack_dll name 'SetConfig';


{$R *.dfm}

procedure TMainForm.btnDumpDoABCTagClick(Sender: TObject);
var
  fs: TFileStream;
begin
  //if dlgSave.Execute then
  if FCurTag is TSwfDoABCTag then
  begin
//    fs := TFileStream.Create(dlgSave.FileName, fmCreate);
//    fs.Write(TSwfDoABCTag(FCurTag).ABCData^, TSwfDoABCTag(FCurTag).ABCSize);
    TSwfDoABCTag(FCurTag).AbcFile.SaveToXml('Dump.xml');
//    fs.Free;
  end;
end;

procedure TMainForm.btnHackClick(Sender: TObject);
var
  str: AnsiString;
begin
  str :=  '    <maps>' +
          '      <map url="http://res.cheng.qq.com/main/QQCity_" topub="1" match="left">' +
          '        <hack package="qqcity.cominterface" class="UserData" func=""/>' +
          '      </map>' +
          '      <map url="http://res.cheng.qq.com/main/fla/Effect_" local="into.swf" match="left"/>' +
          '    </maps>';
end;

procedure TMainForm.btnLoadAbcFileClick(Sender: TObject);
var
//  ms: TMemoryStream;
  FabcFile: TAbcFile;

  procedure LoadIntegers;
  var
    node: TcxTreeListNode;
    i: Int32;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Integers';
    for i := 0 to FabcFile.ConstPool.Integers.Count - 1 do
    begin
      with node.AddChild do
      begin
        Texts[0] := IntToStr(i);
        Texts[1] :=  IntToStr(FabcFile.ConstPool.Integers[i]);
      end;
    end;
  end;

  procedure LoadUIntegers;
  var
    node: TcxTreeListNode;
    i: Int32;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'UIntegers';
    for i := 0 to FabcFile.ConstPool.UIntegers.Count - 1 do
    begin
      with node.AddChild do
      begin
        Texts[0] := IntToStr(i);
        Texts[1] :=  IntToStr(FabcFile.ConstPool.UIntegers[i]);
      end;
    end;
  end;

  procedure LoadDoubles;
  var
    node: TcxTreeListNode;
    i: Int32;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Doubles';
    for i := 0 to FabcFile.ConstPool.Doubles.Count - 1 do
    begin
      with node.AddChild do
      begin
        Texts[0] := IntToStr(i);
        Texts[1] :=  FloatToStr(FabcFile.ConstPool.Doubles[i]);
      end;
    end;
  end;

  function GetString(ID: UInt32): string;
  begin
    Result := UTF8ToString(FabcFile.ConstPool.Strings[ID]);
  end;

  procedure LoadStrings;
  var
    node: TcxTreeListNode;
    i: Int32;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Strings';
    for i := 0 to FabcFile.ConstPool.Strings.Count - 1 do
      with node.AddChild do
      begin
        Texts[0] := IntToStr(i);
        Texts[1] := GetString(i);
      end;
  end;

  function GetNamespace(ID: Int32): TAbcNamespaceInfo;
  begin
    Result := FabcFile.ConstPool.Namespaces[ID];
  end;

  procedure LoadNamespaces;
  var
    node: TcxTreeListNode;
    i: Int32;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Namespaces';
    with node.AddChild do
    begin
      Texts[0] := '0';
      Texts[1] := '*';
    end;

    for i := 1 to FabcFile.ConstPool.Namespaces.Count - 1 do
      with node.AddChild, FabcFile.ConstPool do
      begin
        Texts[0] := IntToStr(i);
        Texts[1] := IntToStr(Namespaces[i].kind);
        Texts[2] := GetString(Namespaces[i].name);
      end;
  end;

  procedure AddNSSet(ANode: TcxTreeListNode; nsset: TAbcNamespaceSetInfo);
  var
    node2: TcxTreeListNode;
    ns: TAbcNamespaceInfo;
    j: Int32;
  begin
    with FabcFile.ConstPool do
    begin
      for j := 0 to nsset.Count - 1 do
      begin
        ns := GetNamespace(nsset[j]);
        node2 := ANode.AddChild;
        node2.Texts[0] := IntToStr(j);
        node2.Texts[1] := IntToStr(ns.kind);
        node2.Texts[2] := GetString(ns.name);
      end;
    end;
  end;

  procedure LoadNSSets;
  var
    node, node1, node2: TcxTreeListNode;
    i, j: Int32;
    ns: TAbcNamespaceInfo;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Namespace Sets';
    with FabcFile.ConstPool do
    begin
      for i := 1 to NamespaceSets.Count - 1 do
      begin
        node1 := node.AddChild;
        node1.Texts[0] := IntToStr(i);
        AddNSSet(node1, NamespaceSets[i]);
      end;
    end;
  end;

  procedure LoadMultinames;
  var
    node, node1: TcxTreeListNode;
    mulName: TAbcMultinameObjectBase;
    ns: TAbcNamespaceInfo;
    i: Int32;
    QName: TAbcMN_QName;
    RTQName: TAbcMN_RTQName;
    RTQNameL: TAbcMN_RTQNameL;
    Multiname: TAbcMN_Multiname;
    MultinameL: TAbcMN_MultinameL;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Multinames';
    node.Texts[1] := 'Type';
    node.Texts[2] := 'QName.ns';
    node.Texts[3] := 'QName.name';

    with FabcFile.ConstPool do
    begin
      for i := 1 to Multinames.Count - 1 do
      begin
        mulName := Multinames[i];
        node1 := node.AddChild;
        node1.Texts[0] := IntToStr(i);
        case mulName.kind of
          CONSTANT_QName, CONSTANT_QNameA:
            begin
              node1.Texts[1] := 'CONSTANT_QName';
              QName := TAbcMN_QName(mulName);
              ns := GetNamespace(QName.ns);
              node1.Texts[2] := Format('ns: id=%d, kind=%d, name=%s', [QName.ns, ns.kind, GetString(ns.name)]);
              node1.Texts[3] := 'QName.name= ' + GetString(QName.name);
            end;
          CONSTANT_RTQName, CONSTANT_RTQNameA:
            begin
              node1.Texts[1] := 'CONSTANT_RTQName';
              RTQName := TAbcMN_RTQName(mulName);
              node1.Texts[2] := GetString(RTQName.name);
            end;
          CONSTANT_RTQNameL, CONSTANT_RTQNameLA:
            begin
              node1.Texts[1] := 'CONSTANT_RTQNameL';
              //RTQNameL := TAbcMN_RTQNameL(mulName.data);
            end;
          CONSTANT_Multiname, CONSTANT_MultinameA:
            begin
              node1.Texts[1] := 'CONSTANT_Multiname';
              Multiname := TAbcMN_Multiname(mulName);
              node1.Texts[2] := GetString(Multiname.name);
              AddNSSet(node1, NamespaceSets[Multiname.NsSet]);
            end;
          CONSTANT_MultinameL, CONSTANT_MultinameLA:
            begin
              node1.Texts[1] := 'CONSTANT_MultinameL';
              MultinameL := TAbcMN_MultinameL(mulName);
              node1.Texts[2] := 'NS Set: ' + IntToStr(MultinameL.nsset);
              AddNSSet(node1, NamespaceSets[MultinameL.nsset]);
            end;
        end;
      end;
    end;
  end;

  procedure LoadMethodSignatures;
  var
    node, node1: TcxTreeListNode;
    i: Int32;
    m: TAbcMethodInfo;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Method Signatures';

    for i := 0 to FabcFile.Methods.Count - 1 do
    begin
      m := FabcFile.Methods[i];
      node1 := node.AddChild;
      node1.Texts[0] := IntToStr(i);
      node1.Texts[1] := GetString(m.Name);
//      node1.Texts[2] :=
    end;
  end;

  function GetMultinameQ(id: Int32): string;
  begin
    if id = 0 then
      Exit('');

    if FabcFile.ConstPool.Multinames[id].Kind = CONSTANT_QName then
      Result := GetString(TAbcMN_QName(FabcFile.ConstPool.Multinames[id]).Name)
    else
      Result := '';
  end;

  procedure LoadInstances;
  var
    node, node1, node2: TcxTreeListNode;
    i, j: Int32;
    inst: TAbcInstance;
    tr: TAbcTrait;
    mth: TAbcMethodInfo;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Instances';

    for i := 0 to FabcFile.Instances.Count-1 do
    begin
      inst := FabcFile.Instances[i];
      node1 := node.AddChild;
      node1.Texts[0] := IntToStr(i);
      node1.Texts[1] := Format('Name: mn=%d, %s', [inst.Name, GetMultinameQ(inst.Name)]);
      node1.Texts[2] := Format('SuperName: mn=%d, %s', [inst.SuperName, GetMultinameQ(inst.SuperName)]);
      node1.Texts[3] := 'Trait count=' + IntToStr(inst.Traits.Count);

      for j := 0 to inst.Traits.Count-1 do
      begin
        tr := inst.Traits[j];
        node2 := node1.AddChild;
        node2.Texts[0] := IntToStr(j);
        node2.Texts[1] := Format('Trait name: mn=%d, %s', [tr.Name, GetMultinameQ(tr.Name)]);
        case tr.Kind of
          CONSTANT_Trait_Method:
          begin
            node2.Texts[2] := 'Method id=' + IntToStr(TAbcTraitMethod(tr.Data).Method);
            if HasValue(tr.Attr, CONSTANT_Trait_ATTR_Override) then
              node2.Texts[3] := 'Override';
          end;
          CONSTANT_Trait_Function:
          begin
            node2.Texts[2] := 'Func id=' + IntToStr(TAbcTraitFunction(tr.Data).Function_);
          end;
          CONSTANT_Trait_Slot:
          begin
            node2.Texts[2] := 'Slot';
          end;
        end;
      end;
    end;
  end;

  procedure LoadClasses;
  var
    node, node1, node2: TcxTreeListNode;
    i, j: Int32;
    cls: TAbcClassInfo;
    tr: TAbcTrait;
  begin
    node := tlABC.Add;
    node.Texts[0] := 'Classes';

    for i := 0 to FabcFile.Classes.Count-1 do
    begin
      cls := FabcFile.Classes[i];
      node1 := node.AddChild;
      node1.Texts[0] := IntToStr(i);
      node1.Texts[3] := 'Trait count=' + IntToStr(cls.Traits.Count);

      for j := 0 to cls.Traits.Count-1 do
      begin
        tr := cls.Traits[j];
        node2 := node1.AddChild;
        node2.Texts[0] := IntToStr(j);
        node2.Texts[1] := Format('Trait name: mn=%d, %s', [tr.Name, GetMultinameQ(tr.Name)]);
        case tr.Kind of
          CONSTANT_Trait_Method:
          begin
            node2.Texts[1] := 'Method id=' + IntToStr(TAbcTraitMethod(tr.Data).Method);
          end;
          CONSTANT_Trait_Function:
          begin
            node2.Texts[1] := 'Func id=' + IntToStr(TAbcTraitFunction(tr.Data).Function_);
          end;
          CONSTANT_Trait_Slot:
          begin
            node2.Texts[2] := 'Slot';
          end;
        end;

      end;
    end;
  end;

begin
  mmLog.Lines.Clear;
  if not (FCurTag is TSwfDoABCTag) then
    Exit;

//  if not dlgOpen.Execute then Exit;

//  FabcFile.LoadFromFile(dlgOpen.FileName);
  FabcFile := (FCurTag as TSwfDoABCTag).AbcFile;
  tlABC.BeginUpdate;
  try
    tlABC.Clear;

    with FabcFile do
    begin
      // version
      tlABC.AddFirst.Texts[0] := Format('Version: %d.%d', [MajorVersion, MinorVersion]);

      // constant pool
      LoadIntegers;
      LoadUIntegers;
      LoadDoubles;
      LoadStrings;
      LoadNamespaces;
      LoadNSSets;
      LoadMultinames;

      //LoadMethodSignatures;
      LoadInstances;
      LoadClasses;
    end;
  finally
    tlABC.EndUpdate;
  end;
end;

procedure TMainForm.btnLoadDumpedSWFClick(Sender: TObject);
begin
  LoadSWF('DumpSWF.swf');
end;

procedure TMainForm.btnLoadRFClick(Sender: TObject);
var
  fn: string;
  pwd: AnsiString;
  i: Int32;
begin
{
  fn := edRFFile.Text;
  pwd := edRFPvtKey.Text;
  Res := GetResFilesByFile(fn, pwd);
  for i := 0 to Res.Files.Count-1 do
  begin
    tvRF.Items.AddChild(nil, Res.Files[i].Name + '   ' + IntToStr(Res.Files[i].Length)).Data := Res.Files[i];
  end;
}
end;

procedure TMainForm.btnLoadSWFClick(Sender: TObject);
begin
  if dlgOpen.Execute then
  begin
    LoadSWF(dlgOpen.FileName);
  end;
end;

procedure TMainForm.btnSaveABCClick(Sender: TObject);
var
  FabcFile: TAbcFile;
begin
  FabcFile := (FCurTag as TSwfDoABCTag).AbcFile;
  FabcFile.SaveToFile('ReassmbleABC.abc');
end;

procedure TMainForm.btnSaveSelectedClick(Sender: TObject);
//var
//  rf: TResFile;
//  fs: TFileStream;
begin
{
  rf := tvRF.Selected.Data;
  fs := TFileStream.Create('tmp_' + rf.Name, fmCreate);
  fs.Write(rf.Data[0], rf.Length);
  fs.Free;
}
end;

procedure TMainForm.btnSaveSWFClick(Sender: TObject);
begin
  FSwf.SaveToFile('ReassmbleSWF.swf');
end;

procedure TMainForm.btnLoadAndHackClick(Sender: TObject);
var
  saRules, saPackFile, saPackPass: AnsiString;
  ms: TMemoryStream;
begin
{
  saRules := AnsiString(mmRules.Text);
  saPackFile := AnsiString(edRFFile2.Text);
  saPackPass := AnsiString(edRFPass2.Text);
  SetConfig(PAnsiChar(saRules), PAnsiChar(saPackFile), PAnsiChar(saPackPass));
  NeedConvert(edSrcUrl.Text);

  if dlgOpen.Execute then
  begin
    ms := TMemoryStream.Create;
    ms.LoadFromFile(dlgOpen.FileName);
    ConvertSwf(ms);
    //ms.SaveToFile(dlgOpen.FileName+'.hacked.swf');
    ms.Free;
    MessageBox(Handle, 'Done.', 'Hint', 64);
  end;
}
end;

procedure TMainForm.Button1Click(Sender: TObject);
begin
  wb.Navigate(edTestUrl.Text);
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  HookApi;
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
  UnHookApi;
end;

procedure TMainForm.Button4Click(Sender: TObject);
var
  saRules, saPackFile, saPackPass: AnsiString;
begin
  saRules := AnsiString(mmRules.Text);
  saPackFile := AnsiString(edRFFile2.Text);
  saPackPass := AnsiString(edRFPass2.Text);
  SetConfig(PAnsiChar(saRules), PAnsiChar(saPackFile), PAnsiChar(saPackPass));
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FTagStream := TMemoryStream.Create;
  //HexEdit.OpenStream(FTagStream, false);

  FSwf := TSwf.Create;
  FTagNames := TStringList.Create;
  FTagNames.LoadFromFile('TagNames.txt');

  cxSplitter1.CloseSplitter;
end;

procedure TMainForm.LoadSWF(const AFileName: string);
var
  i: UInt32;
  item: TListItem;
  s: string;
begin
  FSwf.LoadFromFile(AFileName);
  lbVer.Caption := Format('Version: %d', [FSwf.Header.Version]);
  lvTags.Clear;
  for i := 0 to FSwf.TagCount-1 do
  begin
    s := IntToStr(FSwf.Tags[i].Header.TypeID);
    item := lvTags.Items.Add;
    item.Caption := FTagNames.Values[s];
    if item.Caption = '' then
      item.Caption := '<>UNKNOWN = '+s;
    item.SubItems.Add(s);
  end;
end;

procedure TMainForm.lvTagsClick(Sender: TObject);
var
  item: TListItem;
  tag: TSwfTag;
begin
  item := lvTags.Selected;
  if item = nil then
    Exit;

  tag := FSwf.Tags[item.Index];
  FCurTag := tag;
  pTagInfo.Caption := 'Tag Type: ' + IntToStr(tag.Header.TypeID);
  HexEdit.LoadFromBuffer(tag.Data^, tag.Header.Length);
{
  FTagStream.Clear;
  FTagStream.Write(tag.Data^, tag.Header.Length);
  FTagStream.Position := 0;
  HexEdit.Reload;
}
end;

end.

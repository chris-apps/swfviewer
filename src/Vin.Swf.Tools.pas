unit Vin.Swf.Tools;

interface

uses
  Windows, SysUtils, Classes, msxml,
  vxXmlDom,
  Vin.Swf.Types, Vin.Swf.Consts;

type
  TBitsStream = class(TMemoryStream)
  private
    FPoz: PByte;
    procedure UpdatePosition; inline;
    procedure UpdatePoz; inline;
  public
    procedure LoadFromFile(const AFileName: string);
    procedure LoadFromStream(AStream: TStream);
    procedure ReadBuffer(var ABuffer; const ASize: UInt32); inline;
    function ReadDouble: Double; inline;
    class function ReadEncodedU32(var APoz: PByte): UI32; static;
    class function ReadNBits(const ABuffer; APosition: LongInt; ACount: TBitWidth): longint; static;
    function ReadString(ALength: UInt32): AnsiString;
    function ReadStringWithLength: AnsiString;
    function ReadU16: UInt16; inline;
    function ReadU30: UInt32; inline;
    function ReadU32: UInt32;
    function ReadU8: Byte; inline;
    procedure ResetPosition; inline;
  end;

procedure AddAttrInt(ANode: IXMLDOMNode; AName: string; AValue: Int32);

implementation

uses
  Vin.Swf;

procedure AddAttrInt(ANode: IXMLDOMNode; AName: string; AValue: Int32);
begin
  vxXmlDom.AddAttr(ANode, AName, IntToStr(AValue));
end;


procedure TBitsStream.LoadFromFile(const AFileName: string);
begin
  inherited LoadFromFile(AFileName);
  FPoz := Memory;
end;

procedure TBitsStream.LoadFromStream(AStream: TStream);
begin
  inherited LoadFromStream(AStream);
  FPoz := Memory;
end;

procedure TBitsStream.ReadBuffer(var ABuffer; const ASize: UInt32);
begin
  Move(FPoz^, ABuffer, ASize);
  Inc(FPoz, ASize);
  UpdatePosition;
end;

function TBitsStream.ReadDouble: Double;
begin
  ReadBuffer(Result, 8);
end;

class function TBitsStream.ReadEncodedU32(var APoz: PByte): UI32;
begin
  Result := APoz[0];
  if not bool(Result and $00000080) then
  begin
    Inc(APoz);
    Exit(Result);
  end;
  Result := (Result and $0000007F) or (APoz[1] shl 7);
  if not bool(Result and $00004000) then
  begin
    Inc(APoz, 2);
    Exit(Result);
  end;
  Result := (Result and $00003FFF) or (APoz[2] shl 14);
  if not bool(Result and $00200000) then
  begin
    Inc(APoz, 3);
    Exit(Result);
  end;
  Result := (Result and $001FFFFF) or (APoz[3] shl 21);
  if not bool(Result and $10000000) then
  begin
    Inc(APoz, 4);
    Exit(Result);
  end;
  Result := (Result and $0FFFFFFF) or (APoz[4] shl 28);
  Inc(APoz, 5);
  // return result;
end;

class function TBitsStream.ReadNBits(const ABuffer; APosition: LongInt; ACount: TBitWidth): longint;
var
  I, B: longint;
begin
  Result := 0;
  B := 1 shl (ACount - 1);
  for I := APosition to APosition + ACount - 1 do
  begin
    if (PByteArray(@ABuffer)^[I div 8] and (128 shr (I mod 8))) <> 0 then
      Result := Result or B;
    B := B shr 1;
  end;
end;

function TBitsStream.ReadString(ALength: UInt32): AnsiString;
begin
  if ALength = 0 then
    Exit('');

  SetLength(Result, ALength);
  ReadBuffer(Result[1], ALength);
end;

function TBitsStream.ReadStringWithLength: AnsiString;
var
  len: UInt32;
begin
  len := ReadU30();
  if len = 0 then
    Exit('');

  Result := ReadString(len);
end;

function TBitsStream.ReadU16: UInt16;
begin
  ReadBuffer(Result, 2);
end;

function TBitsStream.ReadU30: UInt32;
begin
  Result := ReadEncodedU32(FPoz);
  UpdatePosition;
end;

function TBitsStream.ReadU32: UInt32;
begin
  Move(FPoz^, Result, 4);
  Inc(FPoz, 4);
  UpdatePosition;
end;

function TBitsStream.ReadU8: Byte;
begin
  ReadBuffer(Result, 1);
end;

procedure TBitsStream.ResetPosition;
begin
  Position := 0;
  FPoz := Memory;
end;

procedure TBitsStream.UpdatePosition;
begin
  Position := UInt32(FPoz) - UInt32(Memory);
end;

procedure TBitsStream.UpdatePoz;
begin
  FPoz := PByte(UInt32(Memory) + Position);
end;

end.

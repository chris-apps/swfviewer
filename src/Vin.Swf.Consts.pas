unit Vin.Swf.Consts;

interface

const
  // Tag Types
  tagEnd = 0;
  tagShowFrame = 1;
  tagDefineShape = 2;
  tagPlaceObject = 4;
  tagRemoveObject = 5;
  tagDefineBits = 6;
  tagDefineButton = 7;
  tagJPEGTables = 8;
  tagSetBackgroundColor = 9;
  tagDefineFont = 10;
  tagDefineText = 11;
  tagDoAction = 12;
  tagDefineFontInfo = 13;
  tagDefineSound = 14;
  tagStartSound = 15;
  tagDefineButtonSound = 17;
  tagSoundStreamHead = 18;
  tagSoundStreamBlock = 19;
  tagDefineBitsLossless = 20;
  tagDefineBitsJPEG2 = 21;
  tagDefineShape2 = 22;
  tagDefineButtonCxform = 23;
  tagProtect = 24;
  tagPlaceObject2 = 26;
  tagRemoveObject2 = 28;
  tagDefineShape3 = 32;
  tagDefineText2 = 33;
  tagDefineButton2 = 34;
  tagDefineBitsJPEG3 = 35;
  tagDefineBitsLossless2 = 36;
  tagDefineEditText = 37;
  tagDefineSprite = 39;
  tagFrameLabel = 43;
  tagSoundStreamHead2 = 45;
  tagDefineMorphShape = 46;
  tagDefineFont2 = 48;
  tagExportAssets = 56;
  tagImportAssets = 57;
  tagEnableDebugger = 58;
  tagDoInitAction = 59;
  tagDefineVideoStream = 60;
  tagVideoFrame = 61;
  tagDefineFontInfo2 = 62;
  tagEnableDebugger2 = 64;
  tagScriptLimits = 65;
  tagSetTabIndex = 66;
  tagFileAttributes = 69;
  tagPlaceObject3 = 70;
  tagImportAssets2 = 71;
  tagDefineFontAlignZones = 73;
  tagCSMTextSettings = 74;
  tagDefineFont3 = 75;
  tagSymbolClass = 76;
  tagMetadata = 77;
  tagDefineScalingGrid = 78;
  tagDoABC = 82;
  tagDefineShape4 = 83;
  tagDefineMorphShape2 = 84;
  tagDefineSceneAndFrameLabelData = 86;
  tagDefineBinaryData = 87;
  tagDefineFontName = 88;
  tagStartSound2 = 89;
  tagDefineBitsJPEG4 = 90;
  tagDefineFont4 = 91;


  specFixed = $FFFF + 1;

  TWIPS = 20;

implementation

end.

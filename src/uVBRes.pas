unit uVBRes;

interface

uses
  Windows, SysUtils, Classes, Generics.Collections,
  vxUtils;

const
  SIGN_RES = 18002;
  EN_PASS_A: AnsiString = 'A9BC';
  EN_PASS_B: AnsiString = '8F46';
  EN_PASS_C: AnsiString = 'C7DC';
  EN_PASS_D: AnsiString = '9BA5';

  vbDirectory = 16;

type
  TResConfig = record
    PrivatePass: AnsiString;
    PublicPass: AnsiString;
    RFPath: AnsiString;
    Ver: AnsiString;
    Major: SmallInt;
    Minor: SmallInt;
    Revision: SmallInt;
  end;

  TResFile = class
  public
    Name: AnsiString;
    Path: AnsiString;
    Attr: LongInt;
    Length: LongInt;
    Pass: AnsiString;
    Data: TBytes;
    Index: LongInt;
    IsEncode: Boolean;
  end;

  TResFiles = class
  public
    Res: SmallInt;
    Ver: SmallInt;
    Major: SmallInt;
    Minor: SmallInt;
    Count: LongInt;
    Pass: AnsiString;
    Files: TList<TResFile>;
    Data: TBytes;
    constructor Create;
    function FindFile(AFileName: AnsiString): TResFile;
  end;

function GetResFile(var filesRes: TResFiles; var Path: AnsiString; Pass: AnsiString = ''): TResFile;

function GetResFiles(var Data: TBytes; var Pass: AnsiString): TResFiles;

function GetResFilesByFile(var Path: string; var Pass: AnsiString): TResFiles;

function ReadBoolean(var Data: TBytes; var Index: LongInt): Boolean;

function ReadLong(var Data: TBytes; var Index: LongInt): LongInt;

function ReadResFile(var Data: TBytes; var Index: LongInt; var Pass: AnsiString; var enPass: AnsiString): TResFile;

function ReadSmallInt(var Data: TBytes; var Index: LongInt): SmallInt;

function ReadString(var Data: TBytes; var Index: LongInt): AnsiString;

procedure Decode(fileRes: TResFile; enPass: AnsiString);

function GetRndStr(len: Int32): AnsiString;

function LoadResFile(var Path: AnsiString; var FullPath: AnsiString): TResFile;

implementation

uses
  StrUtils, AnsiStrings;

function GetResFile(var filesRes: TResFiles; var Path: AnsiString; Pass: AnsiString = ''): TResFile;
var
  i: LongInt;
begin
  Result := nil;
  for i := 0 to filesRes.Count-1 do
  begin
    if AnsiSameStr(filesRes.Files[i].Path, Path) then
    begin
      if not HasValue(filesRes.Files[i].Attr, vbDirectory) and (Pass <> '__OnlyReadData__') then
        Exit(filesRes.Files[i]);
    end;
  end;
end;

function GetResFilesByFile(var Path: string; var Pass: AnsiString): TResFiles;
var
  Data: TBytes;
  fs: TFileStream;
begin
  fs := TFileStream.Create(Path, fmOpenRead or fmShareDenyNone);
  try
    SetLength(Data, fs.Size);
    fs.Read(Data[0], fs.Size);
    Result := GetResFiles(Data, Pass);
  finally
    fs.Free;
  end;
end;

function GetResFiles(var Data: TBytes; var Pass: AnsiString): TResFiles;
var
  Index, i: LongInt;
begin
  Index := 0;
  Result := TResFiles.Create;
  SetLength(Result.Data, Length(Data));
  Move(Data[0], Result.Data[0], Length(Data));
  Result.Res := ReadSmallInt(Data, Index);
  if Result.Res <> SIGN_RES then
    Exit;
  Result.Ver := ReadSmallInt(Data, Index);
  Result.Major := ReadSmallInt(Data, Index);
  Result.Minor := ReadSmallInt(Data, Index);
  Result.Count := ReadLong(Data, Index);
  Result.Pass := ReadString(Data, Index);
  for i := 0 to Result.Count-1 do
    Result.Files.Add(ReadResFile(Data, Index, Pass, Result.Pass));
end;

function ReadBoolean(var Data: TBytes; var Index: LongInt): Boolean;
begin
  if Index >= Length(Data) then
    Exit(False);
  Result := Boolean(Data[Index]);
  Inc(Index);
end;

function ReadSmallInt(var Data: TBytes; var Index: LongInt): SmallInt;
begin
  if Index >= Length(Data) then
    Exit(0);
  Move(Data[Index], Result, 2);
  Inc(Index, 2);
end;

function ReadLong(var Data: TBytes; var Index: LongInt): LongInt;
begin
  if Index >= Length(Data) then
    Exit(0);
  Move(Data[Index], Result, 4);
  Inc(Index, 4);
end;

function ReadString(var Data: TBytes; var Index: LongInt): AnsiString;
var
  s1: AnsiString;
  i, len: LongInt;
begin
  if Index >= Length(Data) then
    Exit('');

  len := Length(Data);
  i := Index;
  while (Data[i] <> 0) and (i < len) do
    Inc(i);
  SetLength(s1, i - Index);
  Move(Data[Index], s1[1], i - Index);
  Index := i + 1;
  Result := s1;
  for i := 1 to Length(Result) do
    if Result[i] = #0 then
      Exit(Copy(Result, 1, i-1));
end;

function ReadResFile(var Data: TBytes; var Index: LongInt; var Pass: AnsiString; var enPass: AnsiString): TResFile;
begin
  if Index >= Length(Data) then
    Exit(nil);

  Result := TResFile.Create;
  Result.Name := ReadString(Data, Index);
  Result.Path := ReadString(Data, Index);
  Result.Attr := ReadLong(Data, Index);
  if HasValue(Result.Attr, vbDirectory) then
    Exit(nil);
  Result.Length := ReadLong(Data, Index);
  Result.Pass := ReadString(Data, Index);
  Result.IsEncode := ReadBoolean(Data, Index);
  SetLength(Result.Data, Result.Length);
  Result.Index := Index;
  if Result.Length > 0 then
    Move(Data[Index], Result.Data[0], Result.Length);
  if Pass <> '' then
    Decode(Result, Pass + enPass);
  Inc(Index, Result.Length);
end;

function LoadResFile(var Path: AnsiString; var FullPath: AnsiString): TResFile;
var
  fs: TFileStream;
begin
  Result := TResFile.Create;
  if FileExists(string(FullPath)) then
    Result.Name := AnsiString(ExtractFilePath(string(FullPath)));
  if Result.Name = '' then
    Exit(nil);
  fs := TFileStream.Create(string(FullPath), fmOpenRead or fmShareDenyNone);
  Result.Path := Path;
  Result.Attr := GetFileAttributesA(PAnsiChar(fullPath));
  if HasValue(Result.Attr, vbDirectory) then
    Exit(nil);
  Result.Length := fs.Size;
  Result.Pass := GetRndStr(16);
  SetLength(Result.Data, fs.Size);
  fs.Read(Result.Data[0], fs.Size);
  fs.Free;
end;

function GetRndStr(len: Int32): AnsiString;
var
  i: Int32;
begin
  Randomize;
  Result := '';
  for i := 1 to len do
    Result := Result + AnsiString(IntToHex(Trunc(16 * Random), 2));
end;

procedure Decode(fileRes: TResFile; enPass: AnsiString);
var
  rndPassLength: LongInt;
  rndPass: TBytes;
  rndPassChr: Byte;
  rndPassIndex: LongInt;
  enPassIndex: LongInt;
  enPassLength: LongInt;
  enPassChr: Byte;
  encChr: Byte;
  enci: LongInt;
  TmpPass: TBytes;
  encBytes: TBytes;
  sa: AnsiString;
begin
  if enPass = '' then
    Exit;

  if not fileRes.IsEncode then
    Exit;

  if fileRes.Length = 0 then
    Exit;

  sa := ReverseString(AnsiString(IntToHex(fileRes.Length, 2))) + fileRes.Pass + EN_PASS_C + EN_PASS_B + EN_PASS_D + EN_PASS_A;
  rndPass := BytesOf(sa);
  rndPassLength := Length(rndPass);
  TmpPass := BytesOf(enPass);
  enPassLength := Length(TmpPass);

  SetLength(encBytes, fileRes.Length);
  Move(fileRes.Data[0], encBytes[0], fileRes.Length);

  enPassIndex := 0;
  rndPassIndex := 0;
  for enci := 0 to Length(encBytes)-1 do
  begin
    encChr := encBytes[enci];
    rndPassChr := rndPass[rndPassIndex];
    Inc(rndPassIndex);
    if rndPassIndex >= rndPassLength then
      rndPassIndex := 0;
    encChr := encChr xor rndPassChr;
    enPassChr := TmpPass[enPassIndex];
    Inc(enPassIndex);
    if enPassIndex >= enPassLength then
      enPassIndex := 0;
    encChr := encChr xor enPassChr;
    if enci > 0 then
      encChr := encChr xor encBytes[enci-1];
    fileRes.Data[enci] := encChr;
  end;
  fileRes.IsEncode := False;
  SetLength(rndPass, 0);
  SetLength(TmpPass, 0);
  SetLength(encBytes, 0);
end;

constructor TResFiles.Create;
begin
  Files := TList<TResFile>.Create;
end;

function TResFiles.FindFile(AFileName: AnsiString): TResFile;
var
  i: Int32;
begin
  Result := nil;
  for i := 0 to Files.Count-1 do
    if AnsiSameText(Files[i].Name, AFileName) then
      Exit(Files[i]);
end;

end.

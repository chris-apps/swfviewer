unit uApiHook;

interface

uses
  SysUtils, Windows, TlHelp32, WinInet, Math, StrUtils, urlmon, Classes, Generics.Collections, RegularExpressions,
  uSwfHacker;

type
  PJmpCode = ^TJmpCode;

  TJmpCode = packed record
    JmpCode: BYTE;
    Address: Pointer;
    MovEAX: array [0 .. 2] of BYTE;
  end;

type
  TApiHookInfo = class
  private
    FCS: TRTLCriticalSection;
    FJmpCode: PJmpCode;
    FOldProc: PJmpCode;
    FLoadLib: Boolean;
    FDllHandle: THandle;
    FHookFun: Pointer;
    m_hProc: DWORD;
    FOldPoint: Cardinal;
    FbHook: Boolean;
    procedure SetPageWrite;
    procedure SetPageReadOnly;
  public
    FAddr: Pointer;
    constructor Create;
    destructor Destroy; override;
    function init(ADllName, AFunName: string; ANewFunPointer: Pointer): Boolean;
    procedure Lock;
    procedure UnLock;
    procedure Hook;
    procedure UnHook;
  end;

function GetInetFile(sAppName: string; const fileURL; var ServerFileBuffer: TByteDynArray): Boolean;
function SplitData(ChangeFileBuffer: TByteDynArray; var TmpBuf: TByteDynArray; AChangerIEDataNum: integer): BOOL;
function MyInternetConnectA(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT;
  lpszUsername: PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD)
  : HINTERNET; stdcall;
function MyHttpOpenRequestA(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar; lpszVersion: PAnsiChar;
  lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD): HINTERNET; stdcall;
function MyInternetReadFile(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD;
  var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;
function MyHttpQueryInfoA(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
  var lpdwReserved: DWORD): BOOL; stdcall;

procedure HookApi;
procedure UnHookApi;

var
  GetServerDataNum, ChangerIEDataNum, IsTargetFiles: integer;
  RequestFileName, HostName: string;
  gHttpOpenRequestaA, gInternetReadFile, gInternetConnectA, gHttpQueryInfoA: TApiHookInfo;
  ServerFileBuffer: TByteDynArray;
  ServerFileStream: TMemoryStream;

  HostNameMap: TDictionary<HINTERNET, string>;
  
implementation

{ TApiHookInfo }

procedure TApiHookInfo.SetPageWrite;
begin
  if Win32PlatForm = VER_PLATFORM_WIN32_NT then // 判断是不是NT.
    VirtualProtect(FAddr, $F, PAGE_EXECUTE_READWRITE, FOldPoint);
end;

procedure TApiHookInfo.SetPageReadOnly;
begin
  if Win32PlatForm = VER_PLATFORM_WIN32_NT then // 判断是不是NT.
    VirtualProtect(FAddr, $F, FOldPoint, FOldPoint);
end;

constructor TApiHookInfo.Create;
begin
  InitializeCriticalSection(FCS);
  New(FJmpCode);
  New(FOldProc);
  FLoadLib := False;
  FDllHandle := 0;
  FHookFun := nil;
  FbHook := False;
end;

destructor TApiHookInfo.Destroy;
begin
  if FbHook then
    UnHook;
  if FLoadLib then
    FreeLibrary(FDllHandle);
  Dispose(FJmpCode);
  Dispose(FOldProc);
  DeleteCriticalSection(FCS);
  inherited;
end;

procedure TApiHookInfo.Hook;
var
  dwSize: Cardinal;
begin
  SetPageWrite;
  WriteProcessMemory(m_hProc, FAddr, FJmpCode, 8, dwSize);
  FbHook := True;
  SetPageReadOnly;
end;

function TApiHookInfo.init(ADllName, AFunName: string; ANewFunPointer: Pointer): Boolean;
var
  dwSize: DWORD;
begin
  FDllHandle := GetModuleHandle(PChar(ADllName));
  if FDllHandle = 0 then
  begin
    FDllHandle := LoadLibrary(PChar(ADllName));
    if FDllHandle = 0 then
    begin
      Result := False;
      Exit;
    end
    else
      FLoadLib := True;
  end;
  // 函数地址
  FAddr := GetProcAddress(FDllHandle, PChar(AFunName));
  if (FAddr = nil) then
  begin
    Result := False;
    Exit;
  end;
  // 当前进程
  m_hProc := GetCurrentProcess();

  if (m_hProc = 0) then
  begin
    Result := False;
    Exit;
  end;
  // 读当前进程中函数地址
  SetPageWrite;
  FJmpCode^.JmpCode := $B8;
  FJmpCode^.MovEAX[0] := $FF;
  FJmpCode^.MovEAX[1] := $E0;
  FJmpCode^.MovEAX[2] := 0;
  ReadProcessMemory(m_hProc, FAddr, FOldProc, 8, dwSize);
  FJmpCode^.Address := ANewFunPointer;
  WriteProcessMemory(m_hProc, FAddr, FJmpCode, 8, dwSize);
  SetPageReadOnly;
  Result := True;
end;

procedure TApiHookInfo.Lock;
begin
  EnterCriticalSection(FCS);
end;

procedure TApiHookInfo.UnHook;
var
  dwSize: DWORD;
begin
  SetPageWrite;
  WriteProcessMemory(m_hProc, FAddr, FOldProc, 8, dwSize);
  FbHook := False;
  SetPageReadOnly;
end;

procedure TApiHookInfo.UnLock;
begin
  LeaveCriticalSection(FCS);
end;

function MyInternetConnectA(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT;
  lpszUsername: PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD)
  : HINTERNET; stdcall;
begin
  gInternetConnectA.Lock;
  try
    gInternetConnectA.UnHook;
    try
      // 你自己的一些代码
      Result := InternetConnectA(hInet, lpszServerName, nServerPort, lpszUsername, lpszPassword, dwService, dwFlags,
        dwContext);
      // 你自己的一些代码
      HostName := string(lpszServerName);
    finally
      gInternetConnectA.Hook;
    end;
  finally
    gInternetConnectA.UnLock;
  end;
end;

function MyHttpOpenRequestA(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar; lpszVersion: PAnsiChar;
  lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD): HINTERNET; stdcall;
var
  url: string;
begin
  gHttpOpenRequestaA.Lock;
  try
    gHttpOpenRequestaA.UnHook;
    try
      // 你自己的一些代码
      Result := HttpOpenRequestA(hConnect, lpszVerb, lpszObjectName, lpszVersion, lpszReferrer, lplpszAcceptTypes,
        dwFlags { or INTERNET_FLAG_NO_CACHE_WRITE } or INTERNET_FLAG_RELOAD, dwContext);
      // 你自己的一些代码
      RequestFileName := string(lpszObjectName);
      url := 'http://' + HostName + RequestFileName;
      if NeedConvert(url) then
        IsTargetFiles := 1
      else
        IsTargetFiles := 0;
      GetServerDataNum := 0;
      ChangerIEDataNum := 0;
      ServerFileStream.Clear;
      SetLength(ServerFileBuffer, 0);
      if IsTargetFiles = 1 then
      begin
        if GetServerDataNum = 0 then
        begin // 判断目标文件下载是否完成
          if Length(CurMapItem.local) > 0 then // 使用本地文件
            TryGetLocalFile(ServerFileStream) // todo: 如果加载失败怎么办？
          else
            GetInetFile('a1s2', url, ServerFileBuffer);
          GetServerDataNum := ServerFileStream.Size; // Length(ServerFileBuffer) ;
          // ByteArrayToFile(ServerFileBuffer, 'E:\3.rar');
          // ServerFileStream.Clear;
          // ServerFileStream.Write(ServerFileBuffer[0], Length(ServerFileBuffer));
          // ConvertSwf(ServerFileBuffer);
          ConvertSwf(ServerFileStream);
          ServerFileStream.Position := 0;
        end;
      end;

      // showmessage(HostName+'       ' +RequestFileName);
    finally
      gHttpOpenRequestaA.Hook;
    end;
  finally
    gHttpOpenRequestaA.UnLock;
  end;
end;

procedure ByteArrayToFile(const ByteArray: TByteDynArray; const FileName: string);
var
  Count: integer;
  F: file of BYTE;
  pTemp: Pointer;
begin
  AssignFile(F, FileName);
  Rewrite(F);
  try
    Count := Length(ByteArray);
    pTemp := @ByteArray[0];
    BlockWrite(F, pTemp^, Count);
  finally
    CloseFile(F);
  end;
end;

function SplitData(ChangeFileBuffer: TByteDynArray; var TmpBuf: TByteDynArray; AChangerIEDataNum: integer): BOOL;
const
  MaxBufSize: integer = 1024;
var
  i, j: integer;
begin
  Result := False;
  if AChangerIEDataNum = 0 then
    Exit;
  j := Length(ChangeFileBuffer) - (AChangerIEDataNum - 1) * MaxBufSize;
  if j >= MaxBufSize then
    j := MaxBufSize;
  for i := 0 to j do
  begin
    SetLength(TmpBuf, j);
    TmpBuf[i] := ChangeFileBuffer[(AChangerIEDataNum - 1) * MaxBufSize + i];
    Result := True;
  end;
end;

function GetInetFile(sAppName: string; const fileURL; var ServerFileBuffer: TByteDynArray): Boolean;
const
  UserAgent =
    'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; Tablet PC 2.0)';
var
  hSession, hURL: HINTERNET;
  Buffer: array [0 .. 1023] of BYTE;
  BufferLen: DWORD;
  i: integer;
begin
  Result := False;
  // ServerFileStream.Clear;
  hSession := InternetOpen(PChar(UserAgent), INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0);
    repeat
      if InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen) then
      begin
        ServerFileStream.Write(Buffer[0], BufferLen);
        {
          i := High(ServerFileBuffer) + 1;
          SetLength(ServerFileBuffer, i + BufferLen);
          CopyMemory(@ServerFileBuffer[i], @Buffer[0], BufferLen);
        }
      end;
    until BufferLen = 0;
    Result := True;
    InternetCloseHandle(hURL);
  finally
    InternetCloseHandle(hSession);
  end;
end;

function MyInternetReadFile(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD;
  var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;
const
  MaxBufSize: integer = 1024;
var
  TmpFileBuffer: TByteDynArray;
  i: DWORD;
  j: real;
  url: string;
begin
  url := 'http://' + HostName + RequestFileName;
  gInternetReadFile.Lock;
  try
    gInternetReadFile.UnHook;
    try
      // 你自己的一些代码
      if IsTargetFiles = 1 then
      begin // 判断是不是目标文件
        (*
          if GetServerDataNum = 0 then begin             //判断目标文件下载是否完成
          if Length(CurMapItem.local) > 0  then   // 使用本地文件
          TryGetLocalFile(ServerFileStream)     //todo: 如果加载失败怎么办？
          else
          GetInetFile('a1s2', url, ServerFileBuffer);
          GetServerDataNum := ServerFileStream.Size;// Length(ServerFileBuffer) ;
          //ByteArrayToFile(ServerFileBuffer, 'E:\3.rar');
          //ServerFileStream.Clear;
          //ServerFileStream.Write(ServerFileBuffer[0], Length(ServerFileBuffer));
          //ConvertSwf(ServerFileBuffer);
          ConvertSwf(ServerFileStream);
          ServerFileStream.Position := 0;
          end;
        *)
        ZeroMemory(lpBuffer, dwNumberOfBytesToRead);
        lpdwNumberOfBytesRead := ServerFileStream.Read(lpBuffer^, dwNumberOfBytesToRead);
        Result := True;
        {
          j := High(ServerFileBuffer) + 1;
          j := int(j / MaxBufSize) + 1;
          if j > ChangerIEDataNum then begin
          ChangerIEDataNum := ChangerIEDataNum + 1;
          SplitData(ServerFileBuffer, TmpFileBuffer, ChangerIEDataNum);
          i := Length(TmpFileBuffer);
          Move(TmpFileBuffer[0], lpBuffer^ , i);
          dwNumberOfBytesToRead := i;
          lpdwNumberOfBytesRead := i;         //没处理完数据就不能等于0
          Result:= True;
          end;
          Finalize(TmpFileBuffer);
        }
      end
      else
        Result := InternetReadFile(hFile, lpBuffer, dwNumberOfBytesToRead, lpdwNumberOfBytesRead);
      // 你自己的一些代码
    finally
      gInternetReadFile.Hook;
    end;
  finally
    gInternetReadFile.UnLock;
  end;
end;

function MyHttpQueryInfoA(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
  var lpdwReserved: DWORD): BOOL; stdcall;
var
  s1, s2: AnsiString;
  LBufLen: DWORD;
  p, i: Int32;
begin
  gHttpQueryInfoA.Lock;
  try
    gHttpQueryInfoA.UnHook;
    try
      Result := HttpQueryInfoA(hRequest, dwInfoLevel, lpvBuffer, lpdwBufferLength, lpdwReserved);
      if not Result then
        Exit;

      if (IsTargetFiles = 1) and (dwInfoLevel = HTTP_QUERY_RAW_HEADERS_CRLF) then
      begin
        s1 := AnsiString(PAnsiChar(lpvBuffer));
        p := Pos('Content-Length: ', s1);
        s2 := Copy(s1, 1, p + 15);
        while s1[p] <> #10 do
          Inc(p);
        s2 := s2 + IntToStr(ServerFileStream.Size) + Copy(s1, p + 1, 255);
        LBufLen := Length(s2);
        if LBufLen > lpdwBufferLength then
        begin
          Result := False;
          lpdwBufferLength := LBufLen + 5;
          SetLastError(ERROR_INSUFFICIENT_BUFFER);
          Exit;
        end;

        Move(s2[1], lpvBuffer^, LBufLen);
        lpdwBufferLength := LBufLen;
      end;
    finally
      gHttpQueryInfoA.Hook;
    end;
  finally
    gHttpQueryInfoA.UnLock;
  end;
end;

procedure HookApi;
begin
  if gHttpOpenRequestaA = nil then
  begin
    gHttpOpenRequestaA := TApiHookInfo.Create;
    gHttpOpenRequestaA.init('wininet.dll', 'HttpOpenRequestA', @MyHttpOpenRequestA);
  end;

  if gInternetReadFile = nil then
  begin
    gInternetReadFile := TApiHookInfo.Create;
    gInternetReadFile.init('wininet.dll', 'InternetReadFile', @MyInternetReadFile);
  end;

  if gInternetConnectA = nil then
  begin
    gInternetConnectA := TApiHookInfo.Create;
    gInternetConnectA.init('wininet.dll', 'InternetConnectA', @MyInternetConnectA);
  end;

  if gHttpQueryInfoA = nil then
  begin
    gHttpQueryInfoA := TApiHookInfo.Create;
    gHttpQueryInfoA.init('wininet.dll', 'HttpQueryInfoA', @MyHttpQueryInfoA);
  end;
end;

procedure UnHookApi;
begin
  if Assigned(gHttpOpenRequestaA) then
  begin
    gHttpOpenRequestaA.UnHook;
    gHttpOpenRequestaA.Free;
  end;

  if Assigned(gInternetReadFile) then
  begin
    gInternetReadFile.UnHook;
    gInternetReadFile.Free;
  end;

  if Assigned(gInternetConnectA) then
  begin
    gInternetConnectA.UnHook;
    gInternetConnectA.Free;
  end;

  if Assigned(gHttpQueryInfoA) then
  begin
    gHttpQueryInfoA.UnHook;
    gHttpQueryInfoA.Free;
  end;
end;

initialization

ServerFileStream := TMemoryStream.Create;

end.

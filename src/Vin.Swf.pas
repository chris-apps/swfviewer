unit Vin.Swf;

interface

uses
  Windows, SysUtils, Classes, Zlib, Generics.Collections, Math,
  Vin.Swf.Types, Vin.Swf.Consts, Vin.Swf.Tools2, Vin.Swf.AbcFile;

type
  TSwfHeader = packed record
    Signature: array [0 .. 2] of AnsiChar; // FWS or CWS
    Version: UI8;
    FileLength: UI32;
    FrameSize: TSwfRect;
    FrameRate: UI8;
    FrameRateRemainder: UI8;
    FrameCount: cardinal;
  end;

  TSwfTagHeader = record
    TypeID: UInt32;
    Length: Int32;
    HeaderLength: UInt32;
  end;

  TSwfTag = class;
  TSwfTagClass = class of TSwfTag;

  TSwfTag = class(TObject)
  private
    FData: PByte;
    FHeader: TSwfTagHeader;
  class var
    FTagClasses: TDictionary<UInt16, TSwfTagClass>;
    class constructor Create;
  public
    constructor Create; virtual;
  protected
    procedure PrepareWrite; virtual;
    class procedure RegisterTagClass(ATagTypeID: UInt16; ATagClass: TSwfTagClass);
  public
    destructor Destroy; override;
    class function CreateTag(ATagTypeID: UInt16): TSwfTag;
    procedure ReadBodyFromStream(ABE: TBitsEngine; ASize: UInt32); virtual;
    procedure WriteBodyToStream(ABE: TBitsEngine); virtual;
    property Data: PByte read FData;
    property Header: TSwfTagHeader read FHeader;
  end;

  TSwf = class(TObject)
  private
  const
    FHeaderSizeBase = 8;
  var
    FHeader: TSwfHeader;
    FRawFrameSize: array of Byte;
    FTags: TList<TSwfTag>;
    function GetTagCount: UInt32;
    function GetTag(index: Integer): TSwfTag;
    procedure SetTag(index: Integer; const Value: TSwfTag);
  public
    constructor Create;
    procedure LoadFromFile(const AFileName: string);
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToFile(const AFileName: string);
    procedure SaveToStream(AStream: TStream);
    property Header: TSwfHeader read FHeader;
    property TagCount: UInt32 read GetTagCount;
    property Tags[index: Integer]: TSwfTag read GetTag write SetTag;
  end;

  TSwfDoABCTag = class(TSwfTag)
  private
    FAbcFile: TAbcFile;
  protected
    procedure PrepareWrite; override;
  public
    constructor Create; override;
    procedure ReadBodyFromStream(ABE: TBitsEngine; ASize: UInt32); override;
    property AbcFile: TAbcFile read FAbcFile;
  end;

implementation

function GetEncodedU32(var Poz: PByte): UI32;
begin
  Result := Poz[0];
  if not bool(Result and $00000080) then
  begin
    Inc(Poz);
    Exit(Result);
  end;
  Result := (Result and $0000007F) or (Poz[1] shl 7);
  if not bool(Result and $00004000) then
  begin
    Inc(Poz, 2);
    Exit(Result);
  end;
  Result := (Result and $00003FFF) or (Poz[2] shl 14);
  if not bool(Result and $00200000) then
  begin
    Inc(Poz, 3);
    Exit(Result);
  end;
  Result := (Result and $001FFFFF) or (Poz[3] shl 21);
  if not bool(Result and $10000000) then
  begin
    Inc(Poz, 4);
    Exit(Result);
  end;
  Result := (Result and $0FFFFFFF) or (Poz[4] shl 28);
  Inc(Poz, 5);
  // return result;
end;

procedure ReadBuffer(var APoz: PByte; var ABuffer; const ASize: UInt32); inline;
begin
  Move(APoz^, ABuffer, ASize);
  Inc(APoz, ASize);
end;

function ReadNBits(const Buffer; Position: longint; Count: TBitWidth): longint;
var
  I, B: longint;
begin
  Result := 0;
  B := 1 shl (Count - 1);
  for I := Position to Position + Count - 1 do
  begin
    if (PByteArray(@Buffer)^[I div 8] and (128 shr (I mod 8))) <> 0 then
      Result := Result or B;
    B := B shr 1;
  end;
end;

constructor TSwf.Create;
begin
  FTags := TList<TSwfTag>.Create();
end;

procedure TSwf.LoadFromFile(const AFileName: string);
var
  fs: TFileStream;
begin
  FTags.Clear;

  fs := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    if fs.Size > 22 then
      LoadFromStream(fs);
  finally
    fs.Free;
  end;
end;

procedure TSwf.LoadFromStream(AStream: TStream);
{$IFDEF DEBUG}
  procedure SaveDecompressedSwf(hdr: TSwfHeader; stm: TStream);
  var
    fs: TFileStream;
    size: UI32;
  begin
    fs := TFileStream.Create('Decompressed.swf', fmCreate);
    fs.Write(AnsiString('FWS'), 3);
    fs.Write(hdr.Version, 1);
    size := 8 + stm.Size;
    fs.Write(size, 4);
    fs.CopyFrom(stm, 0);
    fs.Free;
  end;
{$ENDIF}
var
  Buffer: PByteArray;
  NBitsField: Byte;
  Poz: UInt32;
  ms, tmpms: TMemoryStream;
  zds: TDecompressionStream;
  be: TBitsEngine;
  beSize: Int64;
  Tag: TSwfTag;
  LTagHeader: TSwfTagHeader;
  v: Word;
begin
  SetLength(FRawFrameSize, 0);
  AStream.Position := 0;
  ms := TMemoryStream.Create;
  try
    // 读取头部 8 字节。
    ZeroMemory(@FHeader, SizeOf(TSwfHeader));
    AStream.Read(FHeader, 8);
    if (FHeader.Signature[0] = 'C') and (FHeader.Version >= 6) then   // 压缩的
    begin
      tmpms := TMemoryStream.Create;
      try
        tmpms.CopyFrom(AStream, AStream.Size - 8);
        tmpms.Position := 0;
        zds := TDecompressionStream.Create(tmpms);
        try
          ms.CopyFrom(zds, 0);
          ms.Position := 0;
          {$IFDEF DEBUG}
          SaveDecompressedSwf(FHeader, ms);
          ms.Position := 0;
          {$ENDIF}
        finally
          zds.Free;
        end;
      finally
        tmpms.Free;
      end;
    end
    else
    begin
      // 复制非压缩内容
      ms.CopyFrom(AStream, AStream.Size - 8);
    end;
    Buffer := ms.Memory;
    be := TBitsEngine.Create(ms);

    // 读取剩余头部。
    with FHeader do
    begin
      Poz := 0;
      NBitsField := TBitWidth(ReadNBits(Buffer^, Poz, 5));
      Inc(Poz, 5);
      FrameSize.Xmin := Integer(ReadNBits(Buffer^, Poz, NBitsField));
      Inc(Poz, NBitsField);
      FrameSize.Xmax := Integer(ReadNBits(Buffer^, Poz, NBitsField));
      Inc(Poz, NBitsField);
      FrameSize.Ymin := Integer(ReadNBits(Buffer^, Poz, NBitsField));
      Inc(Poz, NBitsField);
      FrameSize.Ymax := Integer(ReadNBits(Buffer^, Poz, NBitsField));
      Inc(Poz, NBitsField);
      NBitsField := Poz mod 8;
      Poz := Poz div 8;
      if (NBitsField > 0) then
        Inc(Poz);
      SetLength(FRawFrameSize, Poz);
      Move(Buffer^[0], FRawFrameSize[0], Poz);
      FrameRateRemainder := Buffer^[Poz]; // 8.[8]
      FrameRate := Buffer^[Poz + 1];
      FrameCount := Buffer^[Poz + 2] or (Buffer^[Poz + 3] shl 8);
    end;
    Inc(Poz, 4);
    be.BitsStream.Position := Poz;
    beSize := be.BitsStream.Size;

    // 读取 Tags
    repeat
      ZeroMemory(@LTagHeader, SizeOf(TSwfTagHeader));
      v := be.ReadU16;
      LTagHeader.TypeID := v shr 6;
      v := v shl 10;
      LTagHeader.Length := v shr 10;
      if LTagHeader.Length = $3F then
      begin
        LTagHeader.HeaderLength := 6;
        LTagHeader.Length := Integer(be.ReadU32);
      end
      else
        LTagHeader.HeaderLength := 2;

      Tag := TSwfTag.CreateTag(LTagHeader.TypeID);
      Tag.FHeader := LTagHeader;
      Tag.ReadBodyFromStream(be, LTagHeader.Length);
      FTags.Add(Tag);
    until be.BitsStream.Position >= beSize;
  finally
    ms.Free;
  end;
end;

procedure TSwf.SaveToFile(const AFileName: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(AFileName, fmCreate);
  try
    SaveToStream(fs);
  finally
    fs.Free;
  end;
end;

procedure TSwf.SaveToStream(AStream: TStream);
var
  size: UI32;
  i: Int32;
  tag: TSwfTag;
  TagCodeAndLength: UI16;
  be: TBitsEngine;
begin
  be := TBitsEngine.Create(AStream);
  FHeader.Signature[0] := 'F';
  FHeader.FileLength := 0;
  AStream.Size := 0;

  AStream.Write(FHeader.Signature[0], 3);
  AStream.Write(FHeader.Version, 1);
  AStream.Write(FHeader.FileLength, 4);

  AStream.Write(FRawFrameSize[0], Length(FRawFrameSize));
  AStream.Write(FHeader.FrameRateRemainder, 1);
  AStream.Write(FHeader.FrameRate, 1);
  AStream.Write(FHeader.FrameCount, 2);

  for i := 0 to FTags.Count-1 do
  begin
    tag := FTags[i];
    tag.PrepareWrite;

    TagCodeAndLength := tag.Header.TypeID shl 6;
    if tag.Header.HeaderLength = 2 then
      TagCodeAndLength := TagCodeAndLength + tag.Header.Length
    else
      TagCodeAndLength := TagCodeAndLength + 63;
    AStream.Write(TagCodeAndLength, 2);
    if tag.Header.HeaderLength = 6 then
    begin
      AStream.Write(tag.Header.Length, 4);
    end;

    tag.WriteBodyToStream(be);
  end;


  AStream.Position := 4;
  size := AStream.Size;
  AStream.Write(size, 4);
end;

function TSwf.GetTagCount: UInt32;
begin
  Result := FTags.Count;
end;

function TSwf.GetTag(index: Integer): TSwfTag;
begin
  Result := FTags[index];
end;

procedure TSwf.SetTag(index: Integer; const Value: TSwfTag);
begin
  FTags[index] := Value;
end;

class constructor TSwfTag.Create;
begin
  FTagClasses := TDictionary<UInt16, TSwfTagClass>.Create();
end;

constructor TSwfTag.Create;
begin
end;

destructor TSwfTag.Destroy;
begin
  FreeMem(FData);
  inherited;
end;

class function TSwfTag.CreateTag(ATagTypeID: UInt16): TSwfTag;
var
  ATagClass: TSwfTagClass;
begin
  if FTagClasses.TryGetValue(ATagTypeID, ATagClass) then
    Result := ATagClass.Create
  else
    Result := TSwfTag.Create;
end;

procedure TSwfTag.PrepareWrite;
begin
  if FHeader.Length < 63 then
    FHeader.HeaderLength := 2
  else
    FHeader.HeaderLength := 6;
end;

procedure TSwfTag.ReadBodyFromStream(ABE: TBitsEngine; ASize: UInt32);
//var
//  p: Int64;
begin
//  p := ABE.BitsStream.Position;
//  try
    GetMem(FData, ASize);
    ABE.BitsStream.Read(FData^, ASize);
//  finally
//    ABE.BitsStream.Position := p;
//  end;
end;

class procedure TSwfTag.RegisterTagClass(ATagTypeID: UInt16; ATagClass: TSwfTagClass);
begin
  FTagClasses.AddOrSetValue(ATagTypeID, ATagClass);
end;

procedure TSwfTag.WriteBodyToStream(ABE: TBitsEngine);
begin
  ABE.WriteBuffer(FData^, FHeader.Length);
end;

constructor TSwfDoABCTag.Create;
begin
  inherited;
  FAbcFile := TAbcFile.Create;
end;

procedure TSwfDoABCTag.PrepareWrite;
var
  ms: TMemoryStream;
  be: TBitsEngine;
begin
  ms := TMemoryStream.Create;
  be := TBitsEngine.Create(ms);
  try
    FAbcFile.WriteToStream(be);
    FHeader.Length := ms.Size;
    ReallocMem(FData, FHeader.Length);
    Move(ms.Memory^, FData^, FHeader.Length);
  finally
    be.Free;
    ms.Free;
  end;

  inherited;
end;

procedure TSwfDoABCTag.ReadBodyFromStream(ABE: TBitsEngine; ASize: UInt32);
var
  p: Int32;
begin
  p := ABE.BitsStream.Position;
  inherited;
  ABE.BitsStream.Position := p;
  FAbcFile.ReadFromStream(ABE);
end;

initialization
  TSwfTag.RegisterTagClass(82, TSwfDoABCTag);

end.

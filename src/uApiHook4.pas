unit uApiHook4;

interface

uses
  Windows, Messages, classes, SysUtils, wininet, Generics.Collections, RegularExpressions, SyncObjs,
  UnitNt2000Hook, uSwfHacker, vxUtils;

const
  fInternetConnectA = 0;
  fHttpOpenRequestA = 1;
  fHttpAddRequestHeadersA = 2;
  fHttpQueryInfoA = 3;
  fInternetQueryDataAvailable = 4;
  fInternetReadFile = 5;
  fInternetReadFileEx = 6;
  fInternetCloseHandle = 7;
  Trap = true;
  {$WRITEABLECONST ON}
  GlobalHooked: Boolean = False;
  {$WRITEABLECONST OFF}

type
  THostData = record
    HostName: string;
    Port: INTERNET_PORT;
  end;

procedure HookApi4; stdcall;
procedure UnhookApi4; stdcall;
procedure InitDict; stdcall;

implementation

uses
  IdHTTP,
  AnsiStrings;

var
  Hook: array [fInternetConnectA .. fInternetCloseHandle] of THookClass; { API HOOK类 }
  StartHookLock: TRTLCriticalSection;
  INetDownloading: Boolean = false;
  HostDict: TDictionary<UInt32, THostData> = nil; // <hConnect, THostData>
  UrlDict: TDictionary<UInt32, string> = nil; // <hRequest, Url>
  FileStreamDict: TObjectDictionary<UInt32, TMemoryStream> = nil; // <hRequest, File Stream>

function INetGetFile(fileURL: string; ms: TMemoryStream): Boolean;
const
  UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727)';
var
  FHttp: TIdHTTP;
begin
  FHttp := TIdHTTP.Create();
  try
    with FHttp do
    begin
      HandleRedirects := True;
      Request.UserAgent := 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)';
      //IOHandler := TIdHttpAccess(FHttp).MakeImplicitClientHandler;
      //IOHandler.DefStringEncoding := TEncoding.Default;
      try
        Get(fileURL, ms, []);
        Result := True;
      except
        Result := False;
      end;
    end;
  finally
    FHttp.Free;
  end;
end;

function ReplaceHttpHeader(AInputHeader, AParamName, AValue: string): string;
begin
  Result := TRegEx.Replace(AInputHeader, AParamName + ':(.*?)\r', AParamName + ': ' + AValue + #$D);
end;

function MyHttpAddRequestHeadersA(hRequest: HINTERNET; lpszHeaders: PAnsiChar; dwHeadersLength: DWORD;
  dwModifiers: DWORD): BOOL; stdcall;
var
  req: AnsiString;
begin
  Hook[fHttpAddRequestHeadersA].Lock;
  try
    Hook[fHttpAddRequestHeadersA].Unhook;

    if INetDownloading then
    begin
      Result := HttpAddRequestHeadersA(hRequest, lpszHeaders, dwHeadersLength, dwModifiers);
      Exit;
    end;

    req := AnsiString(lpszHeaders);
    if AnsiStrings.AnsiPos('Accept-Encoding', req) > 0 then
    begin
      if req[Length(req)] <> #$D then
      begin
        SetLength(req, Length(req) + 1);
        req[Length(req)] := #$D;
      end;
      req := AnsiString(ReplaceHttpHeader(string(req), 'Accept-Encoding', 'identity'));
      Delete(req, Length(req), 1);
      Result := HttpAddRequestHeadersA(hRequest, PAnsiChar(req), Length(req), dwModifiers or HTTP_ADDREQ_FLAG_ADD or
        HTTP_ADDREQ_FLAG_REPLACE);
    end
    else
      Result := HttpAddRequestHeadersA(hRequest, lpszHeaders, dwHeadersLength, dwModifiers);

  finally
    Hook[fHttpAddRequestHeadersA].Hook;
    Hook[fHttpAddRequestHeadersA].Unlock;
  end;
end;

function MyHttpOpenRequestA(hConnect: HINTERNET; lpszVerb: PAnsiChar; lpszObjectName: PAnsiChar; lpszVersion: PAnsiChar;
  lpszReferrer: PAnsiChar; lplpszAcceptTypes: PLPSTR; dwFlags: DWORD; dwContext: DWORD): HINTERNET; stdcall;
var
  url: string;
  ms: TMemoryStream;
  host: THostData;
begin
  Hook[fHttpOpenRequestA].Lock;
  try
    Hook[fHttpOpenRequestA].Unhook;

    if INetDownloading then
    begin
      Result := HttpOpenRequestA(hConnect, lpszVerb, lpszObjectName, lpszVersion, lpszReferrer, lplpszAcceptTypes,
        dwFlags, dwContext);
      Exit;
    end;

    Result := HttpOpenRequestA(hConnect, lpszVerb, lpszObjectName, lpszVersion, lpszReferrer, lplpszAcceptTypes,
      dwFlags {or INTERNET_FLAG_NO_CACHE_WRITE or INTERNET_FLAG_RELOAD} , dwContext);

    if HostDict.TryGetValue(UInt32(hConnect), host) then
    begin
      url := 'http://' + host.HostName;
      if host.Port <> 80 then
       url := url +':' + IntToStr(host.Port);
      url := url  + string(AnsiString(lpszObjectName));

      if NeedConvert(url) then
      begin
        UrlDict.AddOrSetValue(UInt32(Result), url);
        ms := TMemoryStream.Create;
        if Length(CurMapItem.local) > 0 then // 使用本地文件
          TryGetLocalFile(ms) // todo: 如果加载失败怎么办？
        else
        begin
          INetDownloading := true;
          INetGetFile(url, ms);
          INetDownloading := false;
        end;
        ConvertSwf(ms);
        ms.Position := 0;
        FileStreamDict.AddOrSetValue(UInt32(Result), ms);
      end;
    end;
  finally
    Hook[fHttpOpenRequestA].Hook;
    Hook[fHttpOpenRequestA].Unlock;
  end;
end;

function MyHttpQueryInfoA(hRequest: HINTERNET; dwInfoLevel: DWORD; lpvBuffer: Pointer; var lpdwBufferLength: DWORD;
  var lpdwReserved: DWORD): BOOL; stdcall;
var
  s2: AnsiString;
  s: string;
  LBufLen, LOldBufLen, LInfoLevel, LInfoLevelHigh: DWORD;
  LStm: TMemoryStream;

  procedure ModifyHeader;
  begin
    s := string(AnsiString(PAnsiChar(lpvBuffer)));
    s := ReplaceHttpHeader(s, 'Content-Length', IntToStr(LStm.Size));
    s2 := AnsiString(s);
    LBufLen := Length(s2);
    if LBufLen > LOldBufLen then
    begin
      Result := false;
      lpdwBufferLength := LBufLen + 10;
      SetLastError(ERROR_INSUFFICIENT_BUFFER);
      Exit;
    end;

    Move(s2[1], lpvBuffer^, LBufLen);
    lpdwBufferLength := LBufLen;
  end;

begin
  Hook[fHttpQueryInfoA].Lock;
  try
    Hook[fHttpQueryInfoA].Unhook;

    LOldBufLen := lpdwBufferLength;
    Result := HttpQueryInfoA(hRequest, dwInfoLevel, lpvBuffer, lpdwBufferLength, lpdwReserved);
    if (not Result) or INetDownloading then
      Exit;

    if {UrlDict.ContainsKey(UInt32(hRequest)) and }FileStreamDict.TryGetValue(UInt32(hRequest), LStm) then
    begin
      LInfoLevel := dwInfoLevel and $0000FFFF;
      LInfoLevelHigh := dwInfoLevel and $FFFF0000;
      case LInfoLevel of
        HTTP_QUERY_CONTENT_LENGTH:
          begin
            if HasValue(LInfoLevelHigh, HTTP_QUERY_FLAG_REQUEST_HEADERS shr 4) or
               HasValue(LInfoLevelHigh, HTTP_QUERY_FLAG_REQUEST_HEADERS) then
              PDWORD(lpvBuffer)^ := LStm.Size
            else
              ModifyHeader;
          end;
        HTTP_QUERY_RAW_HEADERS_CRLF:
          ModifyHeader;
      end;
    end;
  finally
    Hook[fHttpQueryInfoA].Hook;
    Hook[fHttpQueryInfoA].Unlock;
  end;
end;

function MyInternetCloseHandle(hInet: HINTERNET): BOOL; stdcall;
begin
  Hook[fInternetCloseHandle].Lock;
  try
    Hook[fInternetCloseHandle].Unhook;

    Result := InternetCloseHandle(hInet);
    if INetDownloading then
      Exit;

    HostDict.Remove(UInt32(hInet));
    UrlDict.Remove(UInt32(hInet));
    FileStreamDict.Remove(UInt32(hInet));

  finally
    Hook[fInternetCloseHandle].Hook;
    Hook[fInternetCloseHandle].Unlock;
  end;
end;

function MyInternetConnectA(hInet: HINTERNET; lpszServerName: PAnsiChar; nServerPort: INTERNET_PORT;
  lpszUsername: PAnsiChar; lpszPassword: PAnsiChar; dwService: DWORD; dwFlags: DWORD; dwContext: DWORD)
  : HINTERNET; stdcall;
var
  hd: THostData;
begin
  Hook[fInternetConnectA].Lock;
  try
    Hook[fInternetConnectA].Unhook;

    Result := InternetConnectA(hInet, lpszServerName, nServerPort, lpszUsername, lpszPassword, dwService, dwFlags,
      dwContext);
    if INetDownloading then
      Exit;

    if not HostDict.ContainsKey(UInt32(Result)) then
    begin
      hd.HostName := string(AnsiString(lpszServerName));
      hd.Port := nServerPort;
      HostDict.Add(UInt32(Result), hd);
    end;
  finally
    Hook[fInternetConnectA].Hook;
    Hook[fInternetConnectA].Unlock;
  end;

end;

function MyInternetQueryDataAvailable(hFile: HINTERNET; var lpdwNumberOfBytesAvailable: DWORD;
  dwFlags, dwContext: DWORD): BOOL; stdcall;
var
  BytesLeft: UInt32;
  LStm: TMemoryStream;
begin
  Hook[fInternetQueryDataAvailable].Lock;
  try
    Hook[fInternetQueryDataAvailable].Unhook;

    Result := InternetQueryDataAvailable(hFile, lpdwNumberOfBytesAvailable, dwFlags, dwContext);
    if INetDownloading then
      Exit;

    if {UrlDict.ContainsKey(UInt32(hFile)) and }FileStreamDict.TryGetValue(UInt32(hFile), LStm) then
    begin
      BytesLeft := LStm.Size - LStm.Position;
      if BytesLeft > lpdwNumberOfBytesAvailable then
        lpdwNumberOfBytesAvailable := 8192
      else
        lpdwNumberOfBytesAvailable := BytesLeft;

      Result := True;
    end;

  finally
    Hook[fInternetQueryDataAvailable].Hook;
    Hook[fInternetQueryDataAvailable].Unlock;
  end;

end;

function MyInternetReadFile(hFile: HINTERNET; lpBuffer: Pointer; dwNumberOfBytesToRead: DWORD;
  var lpdwNumberOfBytesRead: DWORD): BOOL; stdcall;
var
  LStm: TMemoryStream;
begin
  Hook[fInternetReadFile].Lock;
  try
    Hook[fInternetReadFile].Unhook;

    Result := InternetReadFile(hFile, lpBuffer, dwNumberOfBytesToRead, lpdwNumberOfBytesRead);
    if INetDownloading then
      Exit;

    if {UrlDict.ContainsKey(UInt32(hFile)) and }FileStreamDict.TryGetValue(UInt32(hFile), LStm) then
    begin // 判断是不是目标文件
      ZeroMemory(lpBuffer, dwNumberOfBytesToRead);
      lpdwNumberOfBytesRead := LStm.Read(lpBuffer^, dwNumberOfBytesToRead);
      Result := true;
    end;
  finally
    Hook[fInternetReadFile].Hook;
    Hook[fInternetReadFile].Unlock;
  end;

end;

function MyInternetReadFileEx(hFile: HINTERNET;  lpBuffersOut: Pointer; dwFlags: DWORD; dwContext: DWORD): BOOL; stdcall;
var
  LStm: TMemoryStream;
  LBuf: PInternetBuffersA;
begin
  Hook[fInternetReadFileEx].Lock;
  try
    Hook[fInternetReadFileEx].Unhook;

    Result := InternetReadFileExA(hFile, lpBuffersOut, dwFlags, dwContext);
    if INetDownloading then
      Exit;

    if lpBuffersOut = nil then
      Exit(False);

    if {UrlDict.ContainsKey(UInt32(hFile)) and }FileStreamDict.TryGetValue(UInt32(hFile), LStm) then
    begin // 判断是不是目标文件
      LBuf := PInternetBuffersA(lpBuffersOut);
      ZeroMemory(LBuf^.lpvBuffer, LBuf^.dwBufferLength);
      LBuf^.dwBufferLength := LStm.Read(LBuf^.lpvBuffer^, LBuf^.dwBufferLength);
      Result := true;
    end;
(*
    Result := False;
    LBuf := PInternetBuffersA(lpBuffersOut);
    LBuf^.dwBufferLength := 0;
    LBuf^.dwBufferTotal := 0;
*)
  finally
    Hook[fInternetReadFileEx].Hook;
    Hook[fInternetReadFileEx].Unlock;
  end;

end;


{$REGION '钩子管理'}

procedure HookApi4; stdcall;
begin
  StartHookLock.Enter;
  try
    if GlobalHooked then
      Exit;

    if Hook[fInternetConnectA] = nil then
    begin
      Hook[fInternetConnectA] := THookClass.Create(Trap, @InternetConnectA, @MyInternetConnectA);
      Hook[fInternetConnectA].Hook;
    end;

    if Hook[fHttpOpenRequestA] = nil then
    begin
      Hook[fHttpOpenRequestA] := THookClass.Create(Trap, @HttpOpenRequestA, @MyHttpOpenRequestA);
      Hook[fHttpOpenRequestA].Hook;
    end;

    if Hook[fHttpAddRequestHeadersA] = nil then
    begin
      Hook[fHttpAddRequestHeadersA] := THookClass.Create(Trap, @HttpAddRequestHeadersA, @MyHttpAddRequestHeadersA);
      Hook[fHttpAddRequestHeadersA].Hook;
    end;

    if Hook[fHttpQueryInfoA] = nil then
    begin
      Hook[fHttpQueryInfoA] := THookClass.Create(Trap, @HttpQueryInfoA, @MyHttpQueryInfoA);
      Hook[fHttpQueryInfoA].Hook;
    end;

    if Hook[fInternetQueryDataAvailable] = nil then
    begin
      Hook[fInternetQueryDataAvailable] := THookClass.Create(Trap, @InternetQueryDataAvailable, @MyInternetQueryDataAvailable);
      Hook[fInternetQueryDataAvailable].Hook;
    end;

    if Hook[fInternetReadFile] = nil then
    begin
      Hook[fInternetReadFile] := THookClass.Create(Trap, @InternetReadFile, @MyInternetReadFile);
      Hook[fInternetReadFile].Hook;
    end;

    if Hook[fInternetReadFileEx] = nil then
    begin
      Hook[fInternetReadFileEx] := THookClass.Create(Trap, @InternetReadFileExA, @MyInternetReadFileEx);
      Hook[fInternetReadFileEx].Hook;
    end;

    if Hook[fInternetCloseHandle] = nil then
    begin
      Hook[fInternetCloseHandle] := THookClass.Create(Trap, @InternetCloseHandle, @MyInternetCloseHandle);
      Hook[fInternetCloseHandle].Hook;
    end;

    {$WRITEABLECONST ON}
    GlobalHooked := True;
    {$WRITEABLECONST OFF}
  finally
    StartHookLock.Leave;
  end;
end;

procedure UnhookApi4; stdcall;
var
  i: Int32;
begin
  if not GlobalHooked then
    Exit;

  for i := low(Hook) to high(Hook) do
    if Assigned(Hook[i]) then
    begin
      Hook[i].Unhook;
      FreeAndNil(Hook[i]);
    end;

  {$WRITEABLECONST ON}
  GlobalHooked := False;
  {$WRITEABLECONST OFF}
end;

procedure InitDict; stdcall;
begin
  StartHookLock.Initialize;
  HostDict := TDictionary<UInt32, THostData>.Create;
  UrlDict := TDictionary<UInt32, string>.Create;
  FileStreamDict := TObjectDictionary<UInt32, TMemoryStream>.Create([doOwnsValues]);
end;
{$ENDREGION}

initialization
  InitDict;

end.

library LHH;

uses
  SysUtils,
  Classes,
  uSwfHacker in 'uSwfHacker.pas',
  Vin.Swf.AbcFile.Consts in 'Vin.Swf.AbcFile.Consts.pas',
  Vin.Swf.AbcFile in 'Vin.Swf.AbcFile.pas',
  Vin.Swf.Consts in 'Vin.Swf.Consts.pas',
  Vin.Swf in 'Vin.Swf.pas',
  Vin.Swf.Tools in 'Vin.Swf.Tools.pas',
  Vin.Swf.Tools2 in 'Vin.Swf.Tools2.pas',
  Vin.Swf.Types in 'Vin.Swf.Types.pas',
  uApiHook4 in 'uApiHook4.pas',
  UnitNt2000Hook in 'UnitNt2000Hook.pas';

exports
  UnhookApi4 name 'UnHookApi',
  HookApi4 name 'HookApi',
  SetConfig;

begin

end.

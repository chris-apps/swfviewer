program SwfViewer;

uses
  FastMM4,
  Forms,
  vxDebug,
  vxWin32,
  uMainForm in 'uMainForm.pas' {MainForm},
  Vin.Swf in 'Vin.Swf.pas',
  Vin.Swf.Consts in 'Vin.Swf.Consts.pas',
  Vin.Swf.AbcFile in 'Vin.Swf.AbcFile.pas',
  uSwfHacker in 'uSwfHacker.pas',
  Vin.Swf.AbcFile.Consts in 'Vin.Swf.AbcFile.Consts.pas',
  Vin.Swf.Tools in 'Vin.Swf.Tools.pas',
  Vin.Swf.Tools2 in 'Vin.Swf.Tools2.pas',
  Vin.Swf.Types in 'Vin.Swf.Types.pas',
  uVBRes in 'uVBRes.pas';

{$R *.res}

begin
{$IFDEF DEBUG}
//  TConsole.Enabled := True;
//  TConsole.Open;
{$ENDIF}
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'Swf Viewer';
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
